
let shouldBeExtendList = [
  'login',
  'checkSession',
  'authorize',
  'getUserInfo',
  'request',
  'getUserCloudStorage',
  'connectSocket'
]

shouldBeExtendList.map((name) => {
  if (wx[name] == null) {
    return
  }
  wx['ex_' + name] = function (params) {
    return new Promise( (s, f) => {
      params = {
        success: (data) => {
            s(data)
        },
        fail: (err) => {
          f(err)
        },
        ...params
      }

      wx[name](params)
    })
  }
})
