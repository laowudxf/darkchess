
import * as ViewKit from '../../libs/ViewKit/index'

export default class EatedPieceView extends ViewKit.View {
  constructor(isLeft = true) {
    super()
    this.isLeft = isLeft
    this.itemWidth = 20
    this.widthCount = 8
    this.heightCount = 2
    this.width = this.itemWidth * this.widthCount
    this.height = this.itemWidth * this.heightCount
    this.pieces = []
    this.winBlock = null
    this.bgColor = '#c0684b'
    this.cornerRadius = 5
    this.borderWidth = 2
    this.borderColor = '#9e441f'
  }

  addPiece(piece) {
    piece.isHidden = false
    piece.animationAnchorX = 0
    piece.animationAnchorY = 0
    piece.scale.x = 0.4
    piece.scale.y = 0.4
    let count = this.pieces.length
    this.pieces.push(piece)
    this.addSubview(piece)
    // piece.anchorX = this.isLeft ? 0 : 1
    // piece.anchorY = 0
    let x = count % this.widthCount
    let y = Math.floor(count / this.widthCount)
    if (this.isLeft) {
      piece.x = x * this.itemWidth
    } else {
      piece.x = this.width - (x + 1)* this.itemWidth
    }
    piece.y = y * this.itemWidth
    // if (this.pieces.length >= 2 && this.winBlock) {
    if (this.pieces.length >= 16 && this.winBlock) {
        this.winBlock()
    }
  }
}
