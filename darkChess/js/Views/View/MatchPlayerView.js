
import * as ViewKit from '../../libs/ViewKit/index'

export default class MatchPlayerView extends ViewKit.View {
  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 88, 88))
      v.cornerRadius = 44
      v.borderWidth = 6
      v.borderColor = '#fff868'
      v.bgColor = '#4d1211'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, 0)
      this._bgView = v
    }
    return this._bgView
  }

  get avatar () {
    if (this._avatar == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 80, 80))
      v.cornerRadius = 40
      v.maskToBounds = true
      v.position = this.bgView.bounds.center
      this._avatar = v
    }
    return this._avatar
  }

  get nameBg () {
    if (this._nameBg == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.width, 26))
      v.cornerRadius = 13
      v.bgColor = '#4d1211'
      v.anchorX = 0
      v.anchorY = 1
      v.position = new ViewKit.Point(0, this.height)
      this._nameBg = v
    }
    return this._nameBg
  }

  get nameLabel () {
    if (this._nameLabel == null) {
      let v = null
      v = new ViewKit.Label()
      v.textAlign = 'center'
      v.textColor = 'white'
      v.setFont(15)
      v.text = '????'
      v.position = this.nameBg.bounds.center

      this._nameLabel = v
    }
    return this._nameLabel
  }
  setupUI() {

    let views = [this.bgView, this.nameBg]
    this.nameBg.addSubview(this.nameLabel)
    this.bgView.addSubview(this.avatar)
    views.map(x => this.addSubview(x))
  }
}
