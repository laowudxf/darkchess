
import * as ViewKit from '../../libs/ViewKit/index'

export default class SurrenderView extends ViewKit.View {
  get imageView () {
    if (this._imageView == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 441, 335))
      v.position = this.bounds.center
      this._imageView = v
    }
    return this._imageView
  }

  get ensureBtn () {
    if (this._ensureBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 129, 43))
      v.label.text = ''
      v.bgImage = ViewKit.getRes('ensure_btn')
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.height / 2 + 80)
      this._ensureBtn = v
    }
    return this._ensureBtn
  }

  setupUI() {
    let views = [this.imageView, this.ensureBtn]
    views.map(x => this.addSubview(x))
    this.bgColor = 'rgba(0, 0, 0, 0.3)'
  }
}
