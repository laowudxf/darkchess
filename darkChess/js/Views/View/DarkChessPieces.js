import * as ViewKit from '../../libs/ViewKit/index'

export default class DarkChessPieces extends ViewKit.View {

  get titleImageview () {
    if (this._titleImageview == null) {
      let v = null
      v = new ViewKit.ImageView(null, this.bounds)
      this._titleImageview = v
    }
    return this._titleImageview
  }

  get content () {
    return this._content
  }

  set content(value) {
    this._content = value
    // console.log(this.camp);
    let prefix = this.camp.id == 0 ? '红' : '黑'
    let res = ViewKit.getRes('pieces/' + prefix + '-' + value + '@3x')
    // console.log(res);
    this.titleImageview.imageSrc = res
  }

  get state () {
    return this._state
  }

  set state(value) {
    this._state = value
    this.titleImageview.isHidden = value == 0 ? true : false
  }

  setupUI() {
    //0 => 背面 1 => 正面 2 => 不在棋盘 3 => 被吃掉
    this.state = 3
    this.bgColor = '#F4A460'
    this.cornerRadius = this.width / 2
    this.campId = null
    this.addSubview(this.titleImageview)
  }

  jsonable() {
    return  {
      content: this.content,
      state: this.state,
      campId: this.camp.id
    }
  }
}
