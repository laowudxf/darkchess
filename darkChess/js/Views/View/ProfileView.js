import * as ViewKit from '../../libs/ViewKit/index'
import User from '../../DataModel/User'

export default class ProfileView extends ViewKit.View {
  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 301, 288))
      v.bgColor = '#d47f63'
      v.cornerRadius = 14
      v.position = this.center
      v.borderWidth = 6
      v.borderColor = '#9e441f'
      this._bgView = v
    }
    return this._bgView
  }

  get topView () {
    if (this._topView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(3, 3, this.bgView.width - 6, 125))
      v.bgColor = '#b15a39'
      v.cornerRadius = 11
      this._topView = v
    }
    return this._topView
  }

  get closeBtn () {
    if (this._closeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 30, 30))
      v.bgImage = ViewKit.getRes('profile_close')
      v.position = new ViewKit.Point(this.bgView.rect.right, this.bgView.y)
      v.text = ''
      this._closeBtn = v
    }
    return this._closeBtn
  }

  get avatarBg () {
    if (this._avatarBg == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 72, 72))
      v.anchorX = 0
      v.cornerRadius = 36
      v.position = new ViewKit.Point(10, this.topView.height / 2)
      v.bgColor = '#db9880'
      this._avatarBg = v
    }
    return this._avatarBg
  }

  get avatarImageView () {
    if (this._avatarImageView == null) {
      let v = null
      v = new ViewKit.ImageView(User.share().wx_userInfo.avatarUrl, ViewKit.Rect.rect(0, 0, 64, 64))
      v.position = this.avatarBg.bounds.center
      v.cornerRadius = 32
      v.maskToBounds = true
      this._avatarImageView = v
    }
    return this._avatarImageView
  }

  get nameLabel () {
    if (this._nameLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 120, 15))
      v.textColor = 'white'
      v.setFont(15, 'bold')
      v.text = User.share().wx_userInfo.nickName
      v.anchorX = 0
      v.position = new ViewKit.Point(this.avatarBg.rect.right + 10, this.topView.height / 2)
      this._nameLabel = v
    }
    return this._nameLabel
  }

  get bottomView_1 () {
    if (this._bottomView_1 == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.topView.width - 20, 32))
      v.cornerRadius = 16
      v.bgColor = '#dc9981'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.topView.width / 2, this.topView.rect.bottom + 15)
      this._bottomView_1 = v
    }
    return this._bottomView_1
  }

  get bottomView_2 () {
    if (this._bottomView_2 == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.topView.width - 20, 32))
      v.cornerRadius = 16
      v.bgColor = '#dc9981'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.topView.width / 2, this.bottomView_1.rect.bottom + 15)
      this._bottomView_2 = v
    }
    return this._bottomView_2
  }

  get bottomView_3 () {
    if (this._bottomView_3 == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.topView.width - 20, 32))
      v.cornerRadius = 16
      v.bgColor = '#dc9981'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.topView.width / 2, this.bottomView_2.rect.bottom + 15)
      this._bottomView_3 = v
    }
    return this._bottomView_3
  }

  get successLabel () {
    if (this._successLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 98, 15))
      v.setFont(15, 'bold')
      v.textColor = '#b15a39'
      v.text = '胜：' + User.share().victory + '局'
      v.anchorX = 0
      v.position = new ViewKit.Point(10, this.bottomView_1.height / 2)
      this._successLabel = v
    }
    return this._successLabel
  }

  get failLabel () {
    if (this._failLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 98, 15))
      v.setFont(15, 'bold')
      v.textColor = '#b15a39'
      v.text = '负：' + User.share().fail + '局'
      v.anchorX = 0
      v.position = new ViewKit.Point(10, this.bottomView_1.height / 2)
      this._failLabel = v
    }
    return this._failLabel
  }

  get peaceLabel () {
    if (this._peaceLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 98, 15))
      v.setFont(15, 'bold')
      v.textColor = '#b15a39'
      v.text = '和：' + User.share().peace + '局'
      v.anchorX = 0
      v.position = new ViewKit.Point(10, this.bottomView_1.height / 2)
      this._peaceLabel = v
    }
    return this._peaceLabel
  }

  get successRatio () {
    if (this._successRatio == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 98, 15))
      v.setFont(15, 'bold')
      v.textColor = '#b15a39'
      v.text = '胜率：' + User.share().victoryRatio
      v.anchorX = 0
      v.position = new ViewKit.Point(this.failLabel.rect.right, this.bottomView_1.height / 2)
      this._successRatio = v
    }
    return this._successRatio
  }

  setupUI() {
    let views = [this.bgView, this.closeBtn]
    views.map(x => this.addSubview(x))
    this.bgView.addSubview(this.topView)
    this.topView.addSubview(this.avatarBg)
    this.topView.addSubview(this.nameLabel)
    this.topView.addSubview(this.bottomView_1)
    this.topView.addSubview(this.bottomView_2)
    this.topView.addSubview(this.bottomView_3)
    this.avatarBg.addSubview(this.avatarImageView)

    this.bottomView_1.addSubview(this.successLabel)
    this.bottomView_2.addSubview(this.failLabel)
    this.bottomView_2.addSubview(this.successRatio)
    this.bottomView_3.addSubview(this.peaceLabel)
    this.bgColor = 'rgba(0, 0, 0, 0.3)'

  }
}
