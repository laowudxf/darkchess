import * as ViewKit from '../../libs/ViewKit/index'

export default class DarkChessBoardView extends ViewKit.View {

  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('board_bg_1'), this.bounds)
      this._bgView = v
    }
    return this._bgView
  }

  get bgView1 () {
    if (this._bgView1 == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('board_bg'), ViewKit.Rect.rect(0, 0, this.width + 10, this.height + 10))
      v.position = this.bounds.center
      this._bgView1 = v
    }
    return this._bgView1
  }

  setupUI() {
    // this.borderWidth = 10
    // this.borderColor = 'black'
    let views = [this.bgView1, this.bgView]
    views.map(x => this.addSubview(x))

    this.row = 4
    this.colume = 8
    this.itemWidth = this.height / this.row

    this.clickedBlock = null
  }

  // draw(ctx, rect) {
  //   super.draw(ctx, rect)
  //   ctx.beginPath()
  //   let startPoint = this.calcDrawPoint(rect, new ViewKit.Point(0, 0))
  //   for (let i = 0; i < this.row; i++) {
  //     let p = this.calcDrawPoint(rect, new ViewKit.Point(this.width, this.itemWidth * i))
  //     ctx.moveTo(startPoint.x, p.y)
  //     ctx.lineTo(p.x, p.y)
  //   }
  //
  //   for (let i = 0; i < this.colume; i++) {
  //     let p = this.calcDrawPoint(rect, new ViewKit.Point(this.itemWidth * i, this.height))
  //     ctx.moveTo(p.x, startPoint.y)
  //     ctx.lineTo(p.x, p.y)
  //   }
  //   ctx.strokeStyle = 'black'
  //   ctx.lineWidth = 3
  //   ctx.stroke()
  // }

  positionByXY(x, y) {
    return new ViewKit.Point((x + 0.5) * this.itemWidth, (y + 0.5) * this.itemWidth)
  }

  positionByIndex(index) {
    let x = index % this.colume
    let y = Math.floor(index / this.colume)
    return this.positionByXY(x, y)
  }

  xyByPosition(position) {
    let x = Math.floor((position.x - this.x) / this.itemWidth)
    let y = Math.floor((position.y - this.y) / this.itemWidth)
    return [x, y]
  }

  addActionTarget() {
      let tap = new ViewKit.TapGestureRecognzer(this, (ges) => {
        if (this.clickedBlock) {
          this.clickedBlock(this.xyByPosition(ges.startPoint))
        }
      })

      this.addGesture(tap)
  }
}
