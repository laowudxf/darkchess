import * as ViewKit from '../../libs/ViewKit/index'

export default class PlayerView extends ViewKit.View {

  get avatar () {
    if (this._avatar == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('avatar'), ViewKit.Rect.rect(0, 0, 50, 50))
      v.cornerRadius = 25
      v.maskToBounds = true
      this._avatar = v
    }
    return this._avatar
  }

  get avatarBg () {
    if (this._avatarBg == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 54, 54))
      v.anchorY = 0
      v.cornerRadius = v.height / 2
      v.bgColor = '#9e441f'
      this._avatarBg = v
    }
    return this._avatarBg
  }

  get nameLabel () {
    if (this._nameLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 20))
      v.text = 'name'
      v.setFont(14)
      v.textAlign = 'center'
      // v.bgColor = 'gray'
      this._nameLabel = v
    }
    return this._nameLabel
  }

  get gameLostStateLabel () {
    if (this._gameLostStateLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 20))
      v.text = '等待重连中...'
      v.setFont(14)
      v.textAlign = 'left'
      v.textColor = 'white'
      v.isHidden = true
      this._gameLostStateLabel = v
    }
    return this._gameLostStateLabel
  }

  // 0 => left
  get direct () {
    return this._direct
  }

  set direct(value) {
    this._direct = value
    this.nameLabel.anchorY = 1
    if (value == 0) {
      this.avatarBg.anchorX = 0
      this.avatarBg.position = new ViewKit.Point(0, 0)
      this.nameLabel.anchorX = 0
      this.nameLabel.anchorY = 1
      this.nameLabel.textAlign = 'left'
      this.nameLabel.position = new ViewKit.Point(this.avatarBg.width + 10, this.avatarBg.height)

      this.gameLostStateLabel.anchorX = 0
      this.gameLostStateLabel.anchorY = 0
      this.gameLostStateLabel.position = new ViewKit.Point(0, this.avatarBg.rect.bottom)
    } else {
      this.avatarBg.anchorX = 1
      this.avatarBg.position = new ViewKit.Point(this.width, 0)
      this.nameLabel.anchorX = 1
      this.nameLabel.anchorY = 1
      this.nameLabel.textAlign = 'right'
      this.nameLabel.position = new ViewKit.Point(this.avatarBg.x - 10, this.avatarBg.height)
      this.gameLostStateLabel.anchorX = 1
      this.gameLostStateLabel.anchorY = 0
      this.gameLostStateLabel.position = new ViewKit.Point(this.width, this.avatarBg.rect.bottom)
    }
      this.avatar.position = this.avatarBg.bounds.center
  }

  setupUI() {
    let views = [this.avatarBg, this.nameLabel, this.gameLostStateLabel]
    this.avatarBg.addSubview(this.avatar)
    views.map(x => this.addSubview(x))
    this.direct = 0
    this.height = this.avatarBg.height
    this.width = this.nameLabel.rect.right
  }

  get camp () {
    return this._camp
  }

  set camp(value) {
    this._camp = value
    this.updateCamp(value)
  }

  updateCamp(camp) {
    // this.avatar.bgColor = camp.color
    this.avatarBg.bgColor = camp.color
  }
}
