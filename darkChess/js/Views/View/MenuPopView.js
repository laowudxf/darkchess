
import * as ViewKit from '../../libs/ViewKit/index'

export default class MenuPopView extends ViewKit.View {
  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('pop_bg'), ViewKit.Rect.rect(0, 0, 105, 96))
      this._bgView = v
    }
    return this._bgView
  }

  get surrenderBtn () {
    if (this._surrenderBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, this.width - 30, (this.height - 30) / 2))
      v.anchorY = 0
      v.bgColor = '#facd7a'
      v.text = '投降'
      v.label.setFont(17, 'bold')
      v.label.textColor = '#9f4220'
      v.cornerRadius = 5
      v.position = new ViewKit.Point(this.width / 2 + 5, this.height / 2 + 5)
      this._surrenderBtn = v
    }
    return this._surrenderBtn
  }

  get dealBtn () {
    if (this._dealBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, this.width - 30, (this.height - 30) / 2))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 + 5, this.height / 2 - 5)
      v.bgColor = '#facd7a'
      v.label.setFont(17, 'bold')
      v.text = '求和'
      v.label.textColor = '#9f4220'
      v.cornerRadius = 5
      this._dealBtn = v
    }
    return this._dealBtn
  }
    setupUI() {
      this.width = this.bgView.width
      this.height = this.bgView.height
      let views = [this.bgView, this.surrenderBtn, this.dealBtn]
      views.map(x => this.addSubview(x))
    }
}
