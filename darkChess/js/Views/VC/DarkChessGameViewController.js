
import * as ViewKit from '../../libs/ViewKit/index'
import DarkChessGameView from './DarkChessGameView'
import DarkChessPieces from '../View/DarkChessPieces'
import Camp from '../../DataModel/Camp'
import Player from '../../DataModel/Player'
import MenuPopView from '../View/MenuPopView'
import DataBus from '../../DataBus'

export default class DarkChessGameViewController extends ViewKit.ViewController {

  playerCamp(player) {
    let camps = [this.camp1, this.camp2]
    for (let item of camps) {
      console.log(item, player);
      if (item.player.id == player.id) {
        return item
      }
    }
  }

  get menuPopView () {
    if (this._menuPopView == null) {
      let v = null
      v = new MenuPopView(ViewKit.Rect.rect(0, 0, 100, 100))
      v.anchorX = 0
      v.anchorY = 1
      v.position = new ViewKit.Point(this.view.menuBtn.rect.right + 5, this.view.menuBtn.rect.bottom - 20)
      v.isHidden = true
      this._menuPopView = v
    }
    return this._menuPopView
  }

  get cursorImageView () {
    if (this._cursorImageView == null) {
      let v = null
      let width = this.view.board.itemWidth
      v = new ViewKit.ImageView(ViewKit.getRes('cursor'), ViewKit.Rect.rect(0, 0, width, width))
      v.isHidden = true
      this._cursorImageView = v
    }
    return this._cursorImageView
  }

  get starImageView () {
    if (this._starImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('star'), ViewKit.Rect.rect(0, 0, 20, 20))
      v.isHidden = true
      v.anchorX = 1
      v.anchorY = 0
      this._starImageView = v
    }
    return this._starImageView
  }

  get targetItem () {
    return this._targetItem
  }

  set targetItem(value) {
    this._targetItem = value
    if (value) {
      let rect = value.rectInView()
      this.cursorImageView.position = rect.center
    }
    this.cursorImageView.isHidden = value == null
  }

  loadView() {
    this.view = new DarkChessGameView(this.screenRect)
    // this.view = new DarkChessGameView(ViewKit.Rect.rect( -(this.screenRect.height - this.screenRect.width) / 2, (this.screenRect.height - this.screenRect.width) / 2, this.screenRect.height, this.screenRect.width))
  }

  setupUI() {
    this.logicBoard = new LogicBoard()
    this.row = 4
    this.colume = 8
    this.pieces = []
    this.eated_player1 = []
    this.eated_player2 = []
    this.remoteAction = false
    this.createLevels()

    this.player1 = new Player(0)
    this.player2 = new Player(1)

    //game state
    this.targetItem = null
    //end

    let camp0 = new Camp(0)
    let camp1 = new Camp(1)
    this.camp1 = camp0
    this.camp2 = camp1

    this.creatPieces()

    this.view.addSubview(this.menuPopView)
    this.view.addSubview(this.cursorImageView)
    this.view.addSubview(this.starImageView)

    this.reset()
    this.view.menuBtn.isHidden = true
    this.setupWinBlock()

    // setTimeout(() => {
      DataBus.share().registerObserver(this, 'music_bg_open')
      DataBus.share().registerObserver(this, 'openVoice')
    // }, 1000)
  }


  subscribe(o, n, key, ins) {
    switch (key) {
      case 'music_bg_open':
      console.log(n);
      // console.log(this.view.soundSwitch);
      this.view.soundSwitch.bgImage = ViewKit.getRes(n ? 'music_on' : 'music_off')
        break;
        case 'openVoice':
      this.view.soundEffect.bgImage = ViewKit.getRes(n ? 'sound_on' : 'sound_off')
      break

      default:

    }
  }
  setupWinBlock() {
    this.view.eatedView_left.winBlock = () => {
      wx.showModal({
        title: '提示',
        content: 'palyer1 win!',
        showCancel: false,
        success: () => {
          this.parentContainer.pop()
        }
      })
    }

    this.view.eatedView_right.winBlock = () => {
      wx.showModal({
        title: '提示',
        content: 'palyer2 win!',
        showCancel: false,
        success: () => {
          this.parentContainer.pop()
        }
      })
    }
  }

  eated(piece) {
    if (this.currentPlayer == this.player1) {
      this.eated_player1.push(piece)
    } else {
      this.eated_player2.push(piece)
    }
  }

  reset() {
    this.pieces = this.shuffle(this.pieces)
    this.layoutPieces()
    this.currentPlayer = this.player1
  }


  createLevels() {
    let level = {
      '兵': {
        count: 5,
        level:0
      },
      '炮': {
        count: 2,
        level: 1
      },
      '马': {
        count: 2,
        level: 2
      },
      '车': {
        count: 2,
        level: 3
      },
      '象': {
        count: 2,
        level: 4
      },
      '士': {
        count: 2,
        level: 5
      },
      '将': {
        count: 1,
        level: 6,
        id: 1
      },
      '帅': {
        count: 1,
        level: 6,
        id: 0
      },
    }
    this.level = level
  }

  isMyTurn() {
    return true
  }

  sendPiecesAction(ges) {

  }

  sendBoardAction(x, y) {

  }

  showStarWithPiece(piece) {
    this.starImageView.isHidden = false
    let rect = piece.rectInView()
    let width = this.view.board.itemWidth
    rect = rect.copy()
    this.starImageView.position = new ViewKit.Point(rect.right + 5, rect.y - 5)
  }

  setupPiecesAction() {
    this.pieces.map(x => {
      let tap = new ViewKit.TapGestureRecognzer(x, (ges) => {
        if (this.isMyTurn() == false && this.remoteAction == false) {
          return
        }

        if (this.isMyTurn()) {
          this.sendPiecesAction(ges)
        }

        if (x.state == 0) {
          ViewKit.SoundHelper.playSoundWithName('move')
          //star
          this.showStarWithPiece(x)
          //--
          x.state = 1
          if (x.camp.player == null) {
            this.setupCamp(x.camp)
          }
          this.roundFinish()
          return
        }

        if (x.camp.player.id == this.currentPlayer.id) {
          ViewKit.SoundHelper.playSoundWithName('move')
          this.targetItem = x
          return
        }

        if (this.targetItem && this.tryToEat(x)) {
          //sound
          ViewKit.SoundHelper.playSoundWithName('eat')
          //---
          console.log(this.targetItem);
          let boardItem = this.logicBoard.itemWidthPiece(x)
          this.movePieceToItem(this.targetItem, boardItem)
          //star
          this.showStarWithPiece(this.targetItem)
          //--
          x.removeFromParent()
          if (this.currentPlayer == this.player1) {
            this.view.eatedView_left.addPiece(x)
          } else {
            this.view.eatedView_right.addPiece(x)
          }
          this.roundFinish()
        }

      })
      x.addGesture(tap)
    })

  }

  addActionTarget() {
    this.setupPiecesAction()

    this.view.soundSwitch.clickedBlock = () => {
      DataBus.share().music_bg_open = !DataBus.share().music_bg_open
    }

    this.view.soundEffect.clickedBlock = () => {
      DataBus.share().openVoice = !DataBus.share().openVoice
    }

    this.view.backHomeBtn.clickedBlock = () => {
      console.log(this.parentContainer);
      this.parentContainer.pop()
    }

    this.view.menuBtn.clickedBlock = () => {
      this.menuPopView.isHidden = !this.menuPopView.isHidden
    }

    this.view.board.clickedBlock = (xy) => {
      if (this.isMyTurn() == false && this.remoteAction == false) {
        return
      }

      if (this.isMyTurn()) {
        this.sendBoardAction(xy[0], xy[1])
      }

      console.log(xy);
      if (this.targetItem == null) {
        return
      }
      let item = this.logicBoard.itemWidthPiece(this.targetItem)

      let offset_x = Math.abs(item.x - xy[0])
      let offset_y = Math.abs(item.y - xy[1])
      let canMove = ((offset_x + offset_y) == 1)
      if (canMove) {
        let moveToItem = this.logicBoard.itemWithXY(xy[0], xy[1])
        console.log(moveToItem);
        this.movePieceToItem(this.targetItem, moveToItem)
        this.showStarWithPiece(this.targetItem)
        this.roundFinish()
      }
    }
  }

  //rule
  tryToEat(target_2) {
    if (this.targetItem == target_2) {
      return false
    }

    if (target_2.camp.player == this.currentPlayer) {
      return
    }

    if (this.judgeCanTouch(this.targetItem, target_2) == false) {
      return
    }

    if (this.targetItem.content == '炮') {
      return true
    }

    let level1 = this.level[this.targetItem.content]
    let level2 = this.level[target_2.content]

    if (level1.level == 0 && level2.level == 6) {
      return true
    }

    if (level1.level == 6 && level2.level == 0) {
      return false
    }

    return level1.level >= level2.level
  }

  judgeCanTouch(a, b) {
    if (a.content == '炮') {
      let item_a = this.logicBoard.itemWidthPiece(a)
      let item_b = this.logicBoard.itemWidthPiece(b)
      if (item_a.x != item_b.x && item_a.y != item_b.y) {
        return false
      }

      let offset_x = item_b.x - item_a.x
      let offset_y = item_b.y - item_a.y

      let count = Math.max(Math.abs(offset_x), Math.abs(offset_y))
      if (count == 1) {
        return
      }

      let items = []
      for (let i = 1; i < count; i++) {
        let x = item_a.x + (offset_x / count) * i
        let y = item_a.y + (offset_y / count) * i
        console.log(x, y);
        items.push(this.logicBoard.itemWithXY(x, y))
      }
      items = items.filter(x => x.value != null)
      return items.length == 1 ? true : false

      // return false
    } else {
      let item_a = this.logicBoard.itemWidthPiece(a)
      let item_b = this.logicBoard.itemWidthPiece(b)
      if (item_a.near(item_b)) {
        return true
      } else {
        return false
      }
    }
  }

  movePieceToItem(piece, item) {
    let boardItem = this.logicBoard.itemWidthPiece(piece)
    if (boardItem && boardItem.value) {
      boardItem.value.state = 3
      boardItem.value = null
    }
    item.value = piece
    let position = this.view.board.positionByXY(item.x, item.y)
    piece.position = position
  }


  get currentPlayer () {
    return this._currentPlayer
  }

  set currentPlayer(value) {
    this._currentPlayer = value
    this.view.changeArrow(value.id == this.player1.id)
  }

  roundFinish() {
    this.targetItem = null
    this.changePlayer()
  }

  setupCamp(camp) {
    camp.player = this.currentPlayer
    if (camp.id == this.camp1.id) {
      if (this.currentPlayer.id == this.player1.id) {
        this.camp2.player = this.player2
      } else {
        this.camp2.player = this.player1
      }
    } else {
      if (this.currentPlayer.id == this.player1.id) {
        this.camp1.player = this.player2
      } else {
        this.camp1.player = this.player1
      }
    }
    console.log(this.camp1);
    console.log(this.camp2);

    this.view.player1.camp = this.playerCamp(this.player1)
    this.view.player2.camp = this.playerCamp(this.player2)
  }

  changePlayer() {
    if (this.currentPlayer == null) {
      return
    }
    this.currentPlayer = this.currentPlayer.id == this.player1.id ? this.player2 : this.player1
  }

  creatPieces() {

    let camps = [this.camp1, this.camp2]
    camps.map(y => {
      let width = this.view.board.itemWidth
      for (let key in this.level) {
        let x = this.level[key]
        if (x.id != null && x.id != y.id) {
          continue
        }

        let i = x.count
        while (i > 0) {
          let item = new DarkChessPieces(ViewKit.Rect.rect(0, 0, width - 5, width - 5))
          item.camp = y
          item.content = key
          this.view.board.addSubview(item)
          this.pieces.push(item)
          i--
        }
      }
    })
  }

  layoutPieces() {
    this.logicBoard.applyPieces(this.pieces)
    this.logicBoard.board.map(x => {
      x.map(a => {

      let position = this.view.board.positionByXY(a.x, a.y)
      a.value.position = position
      })
    })
  }

  shuffle(arr) {
    var length = arr.length,
    randomIndex,
    temp;
    while (length) {
      randomIndex = Math.floor(Math.random() * (length--));
      temp = arr[randomIndex];
      arr[randomIndex] = arr[length];
      arr[length] = temp
    }
    return arr;
  }

}


class LogicBoard {
  constructor() {
    this.board = []
    for (let i = 0; i < 4; i++) {
      let arr = []
      for (let j = 0; j < 8; j++) {
        let item = new LogicBoardItem()
        item.y = i
        item.x = j
        arr.push(item)
        let left_j = j - 1
        if (left_j >= 0) {
          let left_item = arr[left_j]
          item.left = left_item
          left_item.right = item
        }

        let top_i = i - 1
        if (top_i >= 0) {
          let top_item = this.board[top_i][j]
          item.up = top_item
          top_item.down = item
        }
      }
      this.board.push(arr)
    }

  }

  applyPieces(pieces) {
    let i = 0
    this.board.map(x => {
      x.map(a => {
        if (i >= pieces.length) {
          return
        }
        a.value = pieces[i]
        a.value.state = 0
        i++
      })
    })
  }

  itemWidthPiece(p){
    for (let i of this.board) {
      for (let j of i) {
        if (j.value == p) {
          return j
        }
      }
    }
  }

  itemWithXY(x, y) {
    return this.board[y][x]
  }

  clearBoard() {
    this.board.map(x => {
      x.map(y => {
        if (y.value) {
          y.value.removeFromParent()
          y.value.state = 2
          y.value = null
        }
      })
    })
  }

  jsonable() {
    let result = this.board.map(x => {
      return x.map(y => {
        return y.jsonable()
      })
    })
    return result
  }
}

class LogicBoardItem {
  constructor() {
    this.value = null
    this.x = null
    this.y = null
    this.left = null
    this.right = null
    this.up = null
    this.down = null
  }

  near(item) {
    return this.left == item || this.right == item || this.up == item || this.down == item
  }

  jsonable() {
    return {
      x: this.x,
      y: this.y,
      value: this.value ? this.value.jsonable(): null
    }
  }
}
