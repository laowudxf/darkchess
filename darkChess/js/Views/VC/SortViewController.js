
import * as ViewKit from '../../libs/ViewKit/index'
import SortView from './SortView'
import ShareHelper from '../../Helper/ShareHelper'
import DataBus from '../../DataBus'

export default class SortViewController extends ViewKit.ViewController {


  setupUI() {

  }

  viewWillAppear() {
    DataBus.postMessage({
      method: 'refreshBoard',
      param: {}
    })
    DataBus.share().showRankBoard()
  }


  viewWillDisappear() {
    DataBus.share().hiddenBoard()
  }

  addActionTarget() {
    this.view.returnBtn.clickedBlock = () => {
      this.parentContainer.pop()
    }

    this.view.groupBtn.clickedBlock = () => {
        // wx.aldSendEvent("排行页-查看群排行")
      let shareTicket = DataBus.share().shareTicket
      if (shareTicket == null) {
        ShareHelper.share()
        .then(() => {
          DataBus.postMessage({
            method: 'refreshBoard',
            param: {
              ticket: DataBus.share().shareTicket
            }
          })
        })
        return
      }
      DataBus.postMessage({
        method: 'refreshBoard',
        param: {
          ticket: shareTicket
        }
      })
    }
  }

  loadView() {
    this.view = new SortView(this.screenRect)
  }
}
