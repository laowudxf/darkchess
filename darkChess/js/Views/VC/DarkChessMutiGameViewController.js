
import * as ViewKit from '../../libs/ViewKit/index'
import DarkChessGameView from './DarkChessGameView'
import DarkChessGameViewController from './DarkChessGameViewController'
import DarkChessPieces from '../View/DarkChessPieces'
import Camp from '../../DataModel/Camp'
import Player from '../../DataModel/Player'
import MenuPopView from '../View/MenuPopView'
import SocketHelper from '../../Helper/SocketHelper'
import User from '../../DataModel/User'
import SurrenderView from '../View/SurrenderView'

export default class DarkChessMutiGameViewController extends DarkChessGameViewController {

  setupUI() {
    super.setupUI()
    this.view.menuBtn.isHidden = false
    //0 =>  进入游戏页面  1 => 双方准备好了
    this.setupPlayerInfo(this.opponentUserInfo, this.isHourseOwner ? this.view.player2 : this.view.player1)
    this.setupOwnInfo(this.isHourseOwner ? this.view.player1 : this.view.player2)


    if (this.isHourseOwner) {
      // this.sendCurrentBoard()
    } else {
      // this.reqCurrentBoard()
    }

    this.gameConnect()
    this.bindData()
    setTimeout(() => {
      this.gameConnectBox.beReady()
    }, 1000)

    wx.showLoading({
      title: '准备中'
    })
  }

  setupWinBlock() {
    this.view.eatedView_left.winBlock = () => {
      if (this.gameConnectBox) {
        this.gameConnectBox.overGame()
      }
      if (this.isHourseOwner) {
        this.showSurrenderView()
      } else {
        this.showSurrenderView('fail')
      }
    }

    this.view.eatedView_right.winBlock = () => {
      if (this.gameConnectBox) {
        this.gameConnectBox.overGame()
      }
      this.showSurrenderView(this.isHourseOwner ? 'fail': 'victory')
    }
  }

  gameConnect() {
    this.gameConnectBox.receivePieceAction = (xy) => {
      this.remoteAction = true
      console.log(xy);
      let item = this.logicBoard.itemWithXY(xy.x, xy.y)
      if (item == null) {
        return null
      }
      console.log(item.value);
      item.value.sendTapAction()
      this.remoteAction = false
    }

    this.gameConnectBox.receiveBoardAction = (xy) => {
      this.remoteAction = true
      console.log(xy);
      this.view.board.clickedBlock([xy.x, xy.y])
      // let item = this.logicBoard.itemWithXY(xy.x, xy.y)
      // if (item == null) {
      //   return null
      // }
      // console.log(item.value);
      // item.value.sendTapAction()
      this.remoteAction = false
    }

    this.gameConnectBox.reqPeaceBlock = () => {
      wx.showModal({
        title: '求和',
        content: '对方请求和局',
        cancelText: '拒绝',
        confirmText: '接受',
        success: (res) => {
          if (res.confirm) {
          this.gameConnectBox.accpetPeace()
          } else if (res.cancel) {
          this.gameConnectBox.accpetPeace(false)
          }
        }
      })
    }

  }


  showSurrenderView(imageName = 'victory') {
    let v = new SurrenderView(this.view.bounds)
    v.imageView.imageSrc = ViewKit.getRes(imageName)
    v.ensureBtn.clickedBlock = () => {
      v.removeFromParent()
      if (this.parentContainer) {
        this.parentContainer.pop()
      }
    }
    this.view.addSubview(v)

    switch (imageName) {
      case 'victory':
        User.share().victory += 1
        break;
      case 'fail':
        User.share().fail += 1
        break;
      case 'peace':
        User.share().peace += 1
        break;
      default:

    }
  }

  bindData() {
    // this.gameConnectBox.registerObserver(this, 'isGameOver')
    this.gameConnectBox.registerObserver(this, 'gameState')
  }

  subscribe(o, n, key, ins) {
    console.log(key, n);
    switch (key) {
      case 'gameState':
      switch (n) {
        case 1:
        if (this.isHourseOwner) {
          this.sendCurrentBoard()
        }
        break;
        case 2: //complete
        if (this.isHourseOwner == false) {
            this.resetBoardInfo()
            this.gameConnectBox.receiveBoard()
        }
        this.gameConnectBox.listingHeart()

        wx.hideLoading()
        break
        case 3: //surrender
        this.gameConnectBox.stopSendHeartData()
        this.showSurrenderView()
        break
        case 4: //peace
        this.gameConnectBox.overGame()
        this.showSurrenderView('peace')

        case 5: //对方掉线
        /*
        let otherView = this.isHourseOwner ? this.view.player2 : this.view.player1
        otherView.gameLostStateLabel.isHidden = false
        let count = 20
        let id = setInterval(() => {
          count -= 1
          otherView.gameLostStateLabel.text = '等待重连中...(' + count + 's)'
          if (count == 0) {
              clearInterval(id)
              this.gameConnectBox.gameState = 3
          }
        }, 1000)
        */

        //直接获胜
        wx.showModal({
          title: '提示',
          content: '对方掉线!',
          showCancel: false,
          success: () => {
            this.gameConnectBox.robot()
            this.showSurrenderView()
          }
        })
        break
        default:
        break
      }
      break
      default:
      break
    }
  }

  isMyTurn() {
    if (this.currentPlayer == this.player1) {
      return this.isHourseOwner
    } else {
      return !this.isHourseOwner
    }
  }

  sendPiecesAction(ges) {
    let item = this.logicBoard.itemWidthPiece(ges.target)
    this.gameConnectBox.sendPiecesAction(item.x, item.y)
  }

  sendBoardAction(x, y) {
    this.gameConnectBox.sendBoardAction(x, y)
  }

  sendCurrentBoard() {
    let boardInfo = this.pieces.map(x => x.jsonable())
    console.log(this.boardInfo);
    this.gameConnectBox.send(JSON.stringify({
      boardInfo: this.boardInfo
    }), {
      command: 'sendboard'
    })
  }

  get boardInfo() {
      return this.logicBoard.jsonable()
  }

  reqCurrentBoard() {
      this.send('', {action: 'req_borad'})
  }

  setupPlayerInfo(info, view) {
    console.log(info,view);
    view.avatar.imageSrc = info.avatar
    view.nameLabel.text = info.nickname

  }

  setupOwnInfo(view) {
    view.avatar.imageSrc = User.share().wx_userInfo.avatarUrl
    view.nameLabel.text = User.share().wx_userInfo.nickName

  }

  addActionTarget() {
    super.addActionTarget()
    this.view.backHomeBtn.clickedBlock = () => {
      // this.gameConnectBox.surrender()
      // this.parentContainer.pop()
      wx.showModal({
        title: '提示',
        content: '确定要投降并退出游戏吗?',
        success: (res) => {
          if (res.confirm) {
            User.share().fail += 1
            this.gameConnectBox.surrender()
            this.parentContainer.pop()
          }
        }
      })
    }

    this.menuPopView.surrenderBtn.clickedBlock = () => {
      this.menuPopView.isHidden = true
      wx.showModal({
        title: '提示',
        content: '确定要投降并退出游戏吗?',
        success: (res) => {
          if (res.confirm) {
            User.share().fail += 1
            this.gameConnectBox.surrender()
            this.parentContainer.pop()
          }
        }
      })
    }

    this.menuPopView.dealBtn.clickedBlock = () => {
      this.menuPopView.isHidden = true
      this.gameConnectBox.reqPeace()
    }

  }


  // parseData(data) {
  //   console.log('parse:', data);
  //   let valueData = JSON.parse(data.value)
  //   switch (data.type) {
  //     case 'play': //游戏数据交互
  //     if (this.gameState == 0) {
  //         console.log('bordInfo:', valueData);
  //         this.resetBoardInfo(valueData.boardInfo)
  //     }
  //     break
  //     default:
  //     break
  //
  //   }
  // }

  changePlayer() {
    super.changePlayer()
    this.saveGameInfo()
  }

  saveGameInfo() {
    this.gameConnectBox.send(JSON.stringify({
      boardInfo: this.boardInfo,
      currentPlayer: this.currentPlayer.id
      // left_eat_piece: [],
      // right_eat_piece:[]
    }), {
      command: 'gameInfo'
    })
  }

  resetBoardInfo() {
    console.log(this.gameConnectBox.boardInfo);
    let boardInfo = JSON.parse(this.gameConnectBox.boardInfo)
    this.logicBoard.clearBoard()

    console.log(boardInfo);

    boardInfo.boardInfo.map(x => {
      x.map(y => {
        if (y.value != null) {
          let result = this.pieces.filter(item => y.value.content == item.content && item.camp.id == y.value.campId && item.state == 2)
          if (result.length == 0) {
            console.log('something wrong');
          } else {
            let boardItem = this.logicBoard.itemWithXY(y.x, y.y)
            let piece = result[0]
            this.view.board.addSubview(piece)
            piece.state = 0
            this.movePieceToItem(piece, boardItem)
          }
        }
      })
    })

    // this.pieces.map(x => x.removeFromParent())
    // this.pieces.length = 0

    // let width = this.view.board.itemWidth
    // let id0 = this.camp1.id
    // let id1 = this.camp2.id
    // let camps = {
    //   [id1]: this.camp1,
    //   [id0]: this.camp2,
    // }
    //
    //
    // boardInfo.map(x => {
    //   let item = new DarkChessPieces(ViewKit.Rect.rect(0, 0, width - 5, width - 5))
    //   item.camp = camps[x.campId]
    //   item.content = x.content
    //   this.view.board.addSubview(item)
    //   this.pieces.push(item)
    // })
    // this.layoutPieces()
    // this.setupPiecesAction()
  }
}
