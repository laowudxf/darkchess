import * as ViewKit from '../../libs/ViewKit/index'
import MatchPlayerView from '../View/MatchPlayerView'

export default class SortView extends ViewKit.View {
  get bgImageView () {
    if (this._bgImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('home_bg'), this.bounds)
      this._bgImageView = v
    }
    return this._bgImageView
  }

  get returnBtn () {
    if (this._returnBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(10, 10, 75, 47))
      v.bgImage = ViewKit.getRes('returnBtn')
      v.label.text = ''
      this._returnBtn = v
    }
    return this._returnBtn
  }

  get segmentView () {
    if (this._segmentView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 563 / 2 / 2, 42))
      v.bgColor = '#d27d64'
      v.borderColor = '#9e4421'
      v.borderWidth = 4
      v.cornerRadius = 21
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, 26)
      this._segmentView = v
    }
    return this._segmentView
  }

  get friendBoardBtn () {
    if (this._friendBoardBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, (this.segmentView.width - 10), this.segmentView.height - 10))
      // v.anchorX = 0
      let corner = v.height / 2
      // v.cornerRadiusArr = [corner, 0, 0, corner]
      v.cornerRadius = corner
      v.position = this.segmentView.bounds.center
      // new ViewKit.Point(this.segmentView.width / 2, this.segmentView.height / 2)
      v.bgColor = '#9e441f'
      v.label.text = '好友榜'
      v.label.textColor = 'white'
      this._friendBoardBtn = v
    }
    return this._friendBoardBtn
  }

  get globalBoardBtn () {
    if (this._globalBoardBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, (this.segmentView.width - 10) / 2, this.segmentView.height - 10))
      v.anchorX = 0
      let corner = v.height / 2
      v.cornerRadiusArr = [0, corner, corner, 0]
      v.position = new ViewKit.Point(this.segmentView.width / 2, this.segmentView.height / 2)
      v.bgColor = '#9e441f'
      v.label.text = '世界榜'
      v.label.textColor = 'white'
      this._globalBoardBtn = v
    }
    return this._globalBoardBtn
  }

  get boardBgView () {
    if (this._boardBgView == null) {
      let v = null
      let halfHeight = (this.height / 2) - (this.segmentView.rect.bottom + 10)
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 425, halfHeight * 2))
      v.position = this.center
      v.bgColor = '#d37f63'
      v.cornerRadius = 10
      v.borderWidth = 5
      v.borderColor = '#9e4421'
      this._boardBgView = v
    }
    return this._boardBgView
  }

  get groupBtn () {
    if (this._groupBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 161, 48))
      v.bgImage = ViewKit.getRes('group_sort')
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.boardBgView.rect.bottom + 12)
      v.label.text = ''
      this._groupBtn = v
    }
    return this._groupBtn
  }


    setupUI() {
      let views = [this.bgImageView, this.returnBtn, this.segmentView, this.boardBgView, this.groupBtn]

      this.segmentView.addSubview(this.friendBoardBtn)
      // this.segmentView.addSubview(this.globalBoardBtn)
      views.map(x => {
        this.addSubview(x)
      })

    }
}
