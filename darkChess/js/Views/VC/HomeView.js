import * as ViewKit from '../../libs/ViewKit/index'

export default class HomeView extends ViewKit.View {

  get playBtn () {
    if (this._playBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 161, 48))
      // v.cornerRadius = 10
      // v.bgColor = 'red'
      v.text = ''
      // v.textColor = 'black'
      v.bgImage = ViewKit.getRes('local_game')
      v.anchorX = 1
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 - 10, this.height - 20)
      this._playBtn = v
    }
    return this._playBtn
  }

  get matchBtn () {
    if (this._matchBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 161, 48))
      v.text = ''
      v.bgImage = ViewKit.getRes('match')
      v.anchorX = 0
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 + 10, this.height - 20)
      this._matchBtn = v
    }
    return this._matchBtn
  }

  get sortBordBtn () {
    if (this._sortBordBtn == null) {
      let v = null
      v = new ViewKit.Button( ViewKit.Rect.rect(0, 0, 52, 57))
      v.bgImage = ViewKit.getRes('rankBoard')
      v.anchorX = 1
      v.anchorY = 0
      v.text = ''
      v.position = new ViewKit.Point(this.width - 10, 45)
      this._sortBordBtn = v
    }
    return this._sortBordBtn
  }

  get bgImageView () {
    if (this._bgImageView == null) {
      let v = null
      // v = new ViewKit.ImageView(ViewKit.getRes('home_bg'), this.bounds)
      v = new ViewKit.ImageView(null, this.bounds)
      v.imageName = 'home_bg'
      this._bgImageView = v
    }
    return this._bgImageView
  }


  get bgImageView_1 () {
    if (this._bgImageView_1 == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 509, 277))
      v.imageName = 'home_bg_word'
      v.position = this.center
      this._bgImageView_1 = v
    }
    return this._bgImageView_1
  }

  get profileBtn () {
    if (this._profileBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 71 + 7, 52))
      v.borderWidth = 7
      v.borderColor = '#d37d5f'
      v.bgColor = '#c0684b'
      v.anchorX = 0
      v.anchorY = 0.5
      v.cornerRadiusArr = [0, 26, 26, 0]
      v.text = ''
      v.position = new ViewKit.Point(-7, this.sortBordBtn.rect.center.y)
      this._profileBtn = v
    }
    return this._profileBtn
  }

  get profileAvatar () {
    if (this._profileAvatar == null) {
      let v = null
      let height = this.profileBtn.height - 7
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, height, height))
      v.anchorX = 1
      v.anchorY = 0.5
      v.cornerRadius = height / 2
      v.maskToBounds = true
      v.borderWidth = 3
      // v.borderColor = '#9d5216'
      // v.bgColor = 'white'
      v.position = new ViewKit.Point(this.profileBtn.width - 3.5, this.profileBtn.height / 2)
      this._profileAvatar = v
    }
    return this._profileAvatar
  }

  get profileLabel () {
    if (this._profileLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 15))
      v.text = '个人中心'
      v.textColor = 'white'
      v.lineWidth = 5
      v.textBorderColor = '#9d5216'
      v.setFont(17, 'bold')
      v.anchorX = 0
      v.position = new ViewKit.Point(this.profileBtn.rect.right + 15, this.profileBtn.center.y)
      this._profileLabel = v
    }
    return this._profileLabel
  }


  setupUI () {
    this.bgColor = 'white'
    let views = [this.bgImageView, this.bgImageView_1, this.profileBtn, this.profileLabel, this.sortBordBtn, this.playBtn, this.matchBtn]
    views.map(x => this.addSubview(x))
    this.profileBtn.addSubview(this.profileAvatar)
  }

}
