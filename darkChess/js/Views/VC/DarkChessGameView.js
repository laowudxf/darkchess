import * as ViewKit from '../../libs/ViewKit/index'
import DarkChessBoardView from '../View/DarkChessBoardView'
import PlayerView from '../View/PlayerView'
import EatedPieceView from '../View/EatedPieceView'

export default class DarkChessGameView extends ViewKit.View {

  get backgroundImageView () {
    if (this._backgroundImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('game_bg'), this.bounds)
      this._backgroundImageView = v
    }
    return this._backgroundImageView
  }

  get board () {
    if (this._board == null) {
      let v = null
      v = new DarkChessBoardView(ViewKit.Rect.rect(0, 0, 400, 200))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height - 30)
      this._board = v
    }
    return this._board
  }


  get arrow () {
    if (this._arror == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('arrow'), ViewKit.Rect.rect(0, 0, 50, 50))
      v.position = new ViewKit.Point(this.width / 2, 50)
      v.rotate = Math.PI
      v.isHidden = true
      this._arror = v
    }
    return this._arror
  }

  get player1 () {
    if (this._player1 == null) {
      let v = null
      v = new PlayerView(ViewKit.Rect.rect(0, 0, 150, 100))
      v.anchorX = 0
      v.anchorY = 0
      v.position = new ViewKit.Point(14, 20)
      v.nameLabel.text = 'player1'
      this._player1 = v
    }
    return this._player1
  }

  get player2 () {
    if (this._player2 == null) {
      let v = null
      v = new PlayerView(ViewKit.Rect.rect(0, 0, 150, 100))
      v.anchorX = 1
      v.anchorY = 0
      v.direct = 1
      v.position = new ViewKit.Point(this.width - 14, 20)
      v.nameLabel.text = 'player2'
      this._player2 = v
    }
    return this._player2
  }
  get backHomeBtn () {
    if (this._backHomeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 94, 43))
      v.bgImage = ViewKit.getRes('home_btn')
      v.text = ''
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, 25)
      this._backHomeBtn = v
    }
    return this._backHomeBtn
  }

  get menuBtn () {
    if (this._menuBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 42, 94))
      v.text = ''
      v.bgImage = ViewKit.getRes('menu')
      v.anchorX = 0
      v.anchorY = 1
      v.position = new ViewKit.Point(0, ViewKit.Device.share().isPhoneX ? this.height: this.height - 20)
      this._menuBtn = v
    }
    return this._menuBtn
  }

  get soundEffect () {
    if (this._soundEffet == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 42, 94))
      v.text = ''
      // v.bgImage = ViewKit.getRes('soundEffectSwitch')
      v.anchorX = 1
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width, this.height / 2 + 5)
      this._soundEffet = v
    }
    return this._soundEffet
  }

  get soundSwitch () {
    if (this._soundSwitch == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 42, 94))
      v.text = ''
      // v.bgImage = ViewKit.getRes('sound_on')
      v.anchorX = 1
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width, this.height / 2 - 5)
      this._soundSwitch = v
    }
    return this._soundSwitch
  }

  get leftArrow_on () {
    if (this._leftArrow_on == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('myRound'), ViewKit.Rect.rect(0, 0, 34, 37))
      v.anchorX = 1
      v.position = new ViewKit.Point(this.backHomeBtn.x - 10, this.backHomeBtn.rect.center.y)
      v.rotate = Math.PI
      this._leftArrow_on = v
    }
    return this._leftArrow_on
  }

  get leftArrow_off () {
    if (this._leftArrow_off == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('noMyRound'), ViewKit.Rect.rect(0, 0, 34, 37))
      v.anchorX = 1
      v.position = new ViewKit.Point(this.backHomeBtn.x - 10, this.backHomeBtn.rect.center.y)
      this._leftArrow_off = v
    }
    return this._leftArrow_off
  }

  get rightArrow_on () {
    if (this._rightArrow_on == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('myRound'), ViewKit.Rect.rect(0, 0, 34, 37))
      v.anchorX = 0
      v.position = new ViewKit.Point(this.backHomeBtn.rect.right + 10, this.backHomeBtn.rect.center.y)
      this._rightArrow_on = v
    }
    return this._rightArrow_on
  }

  get rightArrow_off () {
    if (this._rightArrow_off == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('noMyRound'), ViewKit.Rect.rect(0, 0, 34, 37))
      v.anchorX = 0
      v.rotate = Math.PI
      v.position = new ViewKit.Point(this.backHomeBtn.rect.right + 10, this.backHomeBtn.rect.center.y)
      this._rightArrow_off = v
    }
    return this._rightArrow_off
  }

  get eatedView_left () {
    if (this._eatedView_left == null) {
      let v = null
      v = new EatedPieceView()
      v.anchorX = 0
      v.anchorY = 1
      v.position = new ViewKit.Point(this.player1.nameLabel.x,this.player1.nameLabel.rect.y)
      this._eatedView_left = v
    }
    return this._eatedView_left
  }

  get eatedView_right () {
    if (this._eatedView_right== null) {
      let v = null
      v = new EatedPieceView(false)
      v.anchorX = 1
      v.anchorY = 1
      v.position = new ViewKit.Point(this.player2.nameLabel.rect.right, this.player2.nameLabel.rect.y)
      this._eatedView_right= v
    }
    return this._eatedView_right
  }


  setupUI() {
    this.bgColor = 'gray'
    let views = [this.backgroundImageView, this.board, this.player1, this.player2, this.menuBtn, this.soundSwitch, this.soundEffect, this.backHomeBtn,
    this.leftArrow_off, this.leftArrow_on, this.rightArrow_off, this.rightArrow_on]
    views.map(x => this.addSubview(x))
    this.player1.addSubview(this.eatedView_left)
    this.player2.addSubview(this.eatedView_right)
  }


  changeArrow(isPlayer1) {
    // this.arrow.rotate = isPlayer1 ? Math.PI : 0
    this.leftArrow_on.isHidden = isPlayer1 == false
    this.rightArrow_on.isHidden = isPlayer1 == true
  }

}
