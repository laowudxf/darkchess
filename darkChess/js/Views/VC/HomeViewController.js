import * as ViewKit from '../../libs/ViewKit/index'
import HomeView from './HomeView'
import DarkChessGameViewController from './DarkChessGameViewController'
import MatchViewController from './MatchViewController'
import SortViewController from './SortViewController'
import DataBus from '../../DataBus'
import User from '../../DataModel/User'
import ProfileView from '../View/ProfileView'

export default class HomeViewController extends ViewKit.ViewController {
  loadView() {
    this.view = new HomeView(this.screenRect)
    // this.view = new HomeView(ViewKit.Rect.rect(0, 0, 100, 100))
  }

  viewDidAppear() {
    // let width = canvas.width
    // let height = canvas.height
    // canvas.width = height
    // canvas.height = width
  }

  addActionTarget() {
    this.view.playBtn.clickedBlock = () => {
      let vc = new DarkChessGameViewController()
      this.parentContainer.push(vc)
    }

    this.view.matchBtn.clickedBlock = () => {
      let vc = new MatchViewController()
      this.parentContainer.push(vc)
    }

    this.view.profileBtn.clickedBlock = () => {
      let v = new ProfileView(this.view.bounds)
      v.closeBtn.clickedBlock = () => {
        v.removeFromParent()
      }
      this.view.addSubview(v)
    }
    this.view.sortBordBtn.clickedBlock = () => {
        let vc = new SortViewController()
        this.parentContainer.push(vc)
    }
  }

  setupUI() {
    this.getUserInfo()
  }

  hadGetUserInfo() {
    wx.showLoading({
      mask: true,
      title: '登录中'
    })
    this.view.profileAvatar.imageSrc = User.share().wx_userInfo.avatarUrl
    DataBus.share().serverLogin()
    .then(() => {
      wx.hideLoading()
      console.log(User.share().userInfo);
    })
  }

  getUserInfo() {
    wx.ex_getUserInfo()
    .then((userInfo) => {
      console.log(userInfo.userInfo,22222222);
      User.share().wx_userInfo = userInfo.userInfo
      this.hadGetUserInfo()
    })
    .catch(err => {
      console.log(333333333);
      console.log(err, 11111111);
      let v = new ViewKit.View(this.view.bounds)
      v.bgColor = 'black'
      v.alpha = 0.7
      this.view.addSubview(v)
      this.userInfoBtn =  wx.createUserInfoButton({
        type: 'text',
        text: '获取用户信息',
        style: {
          left: this.view.center.x - 100,
          top: this.view.center.y - 20,
          width: 200,
          height: 40,
          lineHeight: 40,
          backgroundColor: '#ff0000',
          color: '#ffffff',
          textAlign: 'center',
          fontSize: 16,
          borderRadius: 4
        }
      })
      this.userInfoBtn.onTap(resp => {
        v.removeFromParent()
        this.getUserInfo()
        this.userInfoBtn.offTap()
        this.userInfoBtn.destroy()
      })
      this.userInfoBtn.show()
    })
  }

}
