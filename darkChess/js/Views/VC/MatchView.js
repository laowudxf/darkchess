
import * as ViewKit from '../../libs/ViewKit/index'
import MatchPlayerView from '../View/MatchPlayerView'

export default class MatchView extends ViewKit.View {

  get bgImageView () {
    if (this._bgImageView == null) {
      let v = null
      v = new ViewKit.ImageView(null, this.bounds)
      v.imageName = 'home_bg'
      this._bgImageView = v
    }
    return this._bgImageView
  }


  get bgImageView_1 () {
    if (this._bgImageView_1 == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('match_bg'), ViewKit.Rect.rect(0, 0, this.width * 0.6, this.height))
      this._bgImageView_1 = v
    }
    return this._bgImageView_1
  }

  get returnBtn () {
    if (this._returnBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(10, 10, 75, 47))
      v.bgImage = ViewKit.getRes('return')
      v.text = ''
      // v.anchorX = 0
      // v.anchorY = 0
      // v.position = new ViewKit.Point()
      this._returnBtn = v
    }
    return this._returnBtn
  }

  get vsImageView () {
    if (this._vsImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('vs'), ViewKit.Rect.rect(0, 0, 165, 146))
      v.position = this.center
      this._vsImageView = v
    }
    return this._vsImageView
  }


  get myInfo () {
    if (this._myInfo == null) {
      let v = null
      v = new MatchPlayerView(ViewKit.Rect.rect(0, 0, 110, 129))
      v.position = new ViewKit.Point(this.width / 4, this.height / 2)
      this._myInfo = v
    }
    return this._myInfo
  }

  get opponentInfo () {
    if (this._opponentInfo == null) {
      let v = null
      v = new MatchPlayerView(ViewKit.Rect.rect(0, 0, 110, 129))
      v.position = new ViewKit.Point(this.width / 4 * 3, this.height / 2)
      this._opponentInfo = v
    }
    return this._opponentInfo
  }

  get matchLabel () {
    if (this._matchLabel == null) {
      let v = null
      v = new ViewKit.Label()
      v.textAlign = 'center'
      v.textColor = 'white'
      v.setFont(20, 'bold')
      v.anchorY = 0
      v.text = '匹配中'
      v.position = new ViewKit.Point(this.width / 2, this.vsImageView.rect.bottom + 20)
      this._matchLabel = v
    }
    return this._matchLabel
  }

  setupUI() {
    this.bgColor = 'white'
    let views = [this.bgImageView, this.bgImageView_1, this.returnBtn, this.vsImageView, this.matchLabel, this.myInfo, this.opponentInfo]

    views.map(x => this.addSubview(x))

  }
}
