import * as ViewKit from '../../libs/ViewKit/index'
import GameView from './GameView'
import DarkChessGameViewController from './DarkChessGameViewController'

export default class GameViewController extends ViewKit.ViewController {
  loadView() {
    this.view = new GameView(this.screenRect)
  }

  get gameVC () {
    if (this._gameVC == null) {
      let v = null
      v = new DarkChessGameViewController()
      this._gameVC = v
    }
    return this._gameVC
  }

  setupUI() {
    this.addChild(this.gameVC)
  }
}
