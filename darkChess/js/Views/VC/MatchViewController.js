
import * as ViewKit from '../../libs/ViewKit/index'
import MatchView from './MatchView'
import User from '../../DataModel/User'
import SocketHelper from '../../Helper/SocketHelper'
import GameConnectBox from '../../Helper/GameConnectBox'
import DarkChessMutiGameViewController from '../VC/DarkChessMutiGameViewController'

export default class MatchViewController extends ViewKit.ViewController {
  loadView() {
    this.view = new MatchView(this.screenRect)
    // this.view = new HomeView(ViewKit.Rect.rect(0, 0, 100, 100))
  }

  get state () {
    return this._state
  }

  set state(value) {
    this._state = value
    if (value == 2) {
      this.view.matchLabel.text = '匹配成功'
    }
  }

  setupUI() {
    this.state = 0

    this.view.myInfo.avatar.imageSrc = User.share().wx_userInfo.avatarUrl
    this.view.myInfo.nameLabel.text = User.share().wx_userInfo.nickName
    this.socket = SocketHelper.share()
    this.match()
  }

  match() {
    this.socket.connect(() => {
      console.log(111111);
      this.socket.send()
    })

    this.socket.onMessageBlock = (data) => {
      let d = null
      try {
        d = JSON.parse(data.data)
        this.parseData(d)
      } catch(e) {
        console.log(e);
      }

    }
    // this.socket.task.onMessage((data) => {
    //   console.log('recive:',data);
    //   let d = null
    //   try {
    //     d = JSON.parse(data.data)
    //     this.parseData(d)
    //   } catch(e) {
    //     console.log(e);
    //   }
    // })
    //
  }

  get matchInterval () {
    return this._matchInterval
  }

  set matchInterval(value) {
    this._matchInterval = value
    this.view.matchLabel.text = '匹配中(' + (20 - value) + 's)'
  }

  parseData(data) {
    console.log(data);
    switch (this.state) {
      case 0: //start matching
      if (data.user == null) {

        this.state = 1
        this.matchingData = data
        this.matchInterval = 0
        this.matchId = setInterval(() => {
          this.matchInterval += 1
          if (this.matchInterval >= 20) {
            clearInterval(this.matchId)
            this.matchId = null
            this.view.returnBtn.clickedBlock()
          }
        }, 1000)
      } else {
        this.matchSuccess(data)
      }
      break;
      case 1:
      // {"room":"55ROOM","user":{"id":44,"nickname":"\u2154\u963f\u5b9d","avatar":"https:\/\/wx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTJWBEKUicbT6qicYq0FPPKiaafxQSyJt1B6F6SrfffFb0aphlVWOlyyga2FCqE0xW8H7SSOmNfMbfuWg\/132","address":'温州'}}
      //匹配成功
      this.matchSuccess(data)
      default:
    }
  }

  matchSuccess(data) {
    if (this.matchId) {
      clearInterval(this.matchId)
    }
    this.state = 2
    this.matchingData = data
    this.state = 2
    this.view.opponentInfo.avatar.imageSrc = this.matchingData.user.avatar
    this.view.opponentInfo.nameLabel.text = this.matchingData.user.nickname
    setTimeout(() => {
      let vc = new DarkChessMutiGameViewController()
      vc.gameConnectBox = new GameConnectBox(SocketHelper.share())
      vc.gameConnectBox.matchingData = this.matchingData

      vc.isHourseOwner = (data.room == (User.share().userInfo.id + 'ROOM'))

      console.log(vc.isHourseOwner, data.room, 111111111111);

      vc.opponentUserInfo = data.user
      vc.matchingData = this.matchingData
      let nav = this.parentContainer
      nav.pop()
      nav.push(vc)
    }, 2000)
  }

  addActionTarget() {
    this.view.returnBtn.clickedBlock = () => {
      if (this.state != 0) {
        this.socket.finishGame(this.matchingData.room)
      } else {
        console.log('no room');
      }
      if (this.matchId) {
        clearInterval(this.matchId)
      }
      this.parentContainer.pop()
    }
  }
}
