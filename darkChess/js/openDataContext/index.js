
import * as ViewKit from './libs/ViewKit/index'
import DataBus from './DataBus'
import BoardView from './Views/BoardView'
import ChaseView from './Views/ChaseView'

class  Controller {
  constructor() {
    this.app = new ViewKit.App({canvas: sharedCanvas})
    this.app.refreshTimeInterval = 60
    this.setupUI()

    DataBus.share().getFirendScore()
    .then(data => {
    })
  }

  get boardView () {
    if (this._boardView == null) {
      let v = null
      v = new BoardView(this.app.window.bounds)
      this._boardView = v
    }
    return this._boardView
  }

  get chaseView () {
    if (this._chaseView == null) {
      let v = null
      v = new ChaseView(this.app.window.bounds)
      this._chaseView = v
    }
    return this._chaseView
  }

  setupUI() {
    console.log(sharedCanvas.width, sharedCanvas.height);
    // this.app.rootView = this.boardView
    // this.app.rootView = this.chaseView
  }

  saveHighestScore(param) {
    DataBus.share().saveHighestScore(param.score)
  }

  setOpenId(param) {
    DataBus.share().openId = param.openId
    console.log('openId:' + param.openId);
    console.log(DataBus.share().openId);
    this.boardView.refresh()
  }

  refreshBoard(param) {

    DataBus.share().getFirendScore(param.ticket).then(data => {
      this.boardView.dataModel = data
    })
  }

  refreshChaseInfo(param) {

    DataBus.share().getChaseUserInfo(param.score).then(data => {
      this.chaseView.dataModel = data
    })
  }

  shouldRefresh(param) {
      this.app.shouldRefresh = param.shouldRefresh
  }

  setRootView(param) {
    console.log(param);
    if (param && param.index != null){
      let map = [this.boardView, this.chaseView]
      console.log(map[param.index]);
      this.app.rootView = map[param.index]
      if (param.index == 1) {
        this.app.refreshView = this.app.rootView
        this.app.refreshTimeInterval = 60
      } else {
        this.app.refreshView = this.app.window
        this.app.refreshTimeInterval = null
      }
    }
  }
}

let controller = new Controller()

wx.onMessage(data => {
  console.log(data)
  let method = data['method']
  let param = data['param']
  switch (method) {
    case 'shouldReDraw':
    controller = new Controller()
    break;
    case 'touchEvent':
    let event = param.event
    let type = param.type
    controller.view.testHit(event)
    break
    default:
    console.log('method: ' + method);
    if (!controller[method]) {
      return
    }
    console.log('call controller method:' + method);
    controller[method](param)
    return
  }
})
