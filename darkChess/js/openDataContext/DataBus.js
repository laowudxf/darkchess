let dataBusInstance

export default class DataBus {
  constructor() {
    if (dataBusInstance) {
      return dataBusInstance
    }

    this.isBoardShow = false
    dataBusInstance = this
  }

  static share() {
      return new DataBus()
  }

  saveHighestScore(highestScore) {
    wx.setUserCloudStorage({
      KVDataList: [{key: 'highestScore', value: String(highestScore)}],
      success: res => {
        console.log(res);
      },
      fail: res => {
        console.log(res);
      }
    })
  }

   getFirendScore(shareTicket) {
     return new Promise((s, f) => {
       let param =  {
         keyList: ['highestScore'],
         success: (data) => {
           let d = data.data
           console.log(d);
           let result = d.sort((a, b) => {
             if (a.KVDataList.length == 0) {
               return true
             }

             if (b.KVDataList.length == 0) {
               return false
             }
             
             let f = parseInt(a.KVDataList[0].value) < parseInt(b.KVDataList[0].value)
             return f
           })
           result.map((x, i) => x.sort = i)
           // if (result.length > 20) {
           //   result.length = 20
           // }
           s(result)
         },
         fail: err => {
           console.log(err);
           f(err)
         }
       }

       if (shareTicket) {
         param['shareTicket'] = shareTicket
         wx.getGroupCloudStorage(param)
       } else {
         wx.getFriendCloudStorage(param)
       }
     })
   }

   getChaseUserInfo(selfScore) {
     return new Promise((s, f) => {
       let param =  {
         keyList: ['highestScore'],
         success: (data) => {
           let d = data.data
           let result = d.sort((a, b) => {
             let f = parseInt(a.KVDataList[0].value) < parseInt(b.KVDataList[0].value)
             return f
           })

           result.map((x, i) => x.sort = i)

           let chaseResult = null
           if (selfScore == 0) {
             return s(chaseResult)
           }

           for (let i = 0; i < result.length; i++) {
             let offset = selfScore - parseInt(result[i].KVDataList[0].value)
             if (offset == 0 && (i - 1) >= 0) {
               offset = result[i - 1].KVDataList[0].value - selfScore
               chaseResult = {
                 offset,
                 // userInfo: result[i - 1]
                 userInfo: result[i - 1]
               }
               s(chaseResult)
               return
             }
           }
           s(chaseResult)
         },
         fail: err => {
           console.log(err);
           f(err)
         }
       }

       wx.getFriendCloudStorage(param)
     })
   }

 }
