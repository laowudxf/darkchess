import * as ViewKit from '../libs/ViewKit/index'

export default class BoardItemView extends ViewKit.View {

  get sortImageView () {
    if (!this._sortImageView) {
      this._sortImageView = new ViewKit.ImageView(null , ViewKit.Rect.rect(0, 0, 30, 35))
      this._sortImageView.anchorX = 0
      this._sortImageView.position = new ViewKit.Point(5, this.rect.height / 2)
    }
    return this._sortImageView
  }

  get bgImageView () {
    if (!this._bgImageView) {
      this._bgImageView = new ViewKit.ImageView(null , this.rect.bounds)
    }
    return this._bgImageView

  }

  get avatarImageView () {
    if (!this._avatarImageView) {
      let width = this.rect.height * 0.7
      console.log('width:' + width);
      let v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, width, width))
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      v.borderWidth = 5
      this._avatarImageView = v
    }
    return this._avatarImageView
  }

  get nameLabel () {
    if (!this._nameLabel) {
      this._nameLabel = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 30))
      this._nameLabel.setFont(14, 'bold')
      this._nameLabel.textColor = '#9e441f'
    }
    return this._nameLabel
  }

  get sortLabel () {
    if (!this._sortLabel) {
      this._sortLabel = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 30, 30))
      this._sortLabel.setFont(14)
      this._sortLabel.textColor = '#ffffff'
      this._sortLabel.textAlign = 'center'
    }
    return this._sortLabel
  }

  get sortBgView () {
    if (!this._sortBgView) {
      this._sortBgView = new ViewKit.View(ViewKit.Rect.rect(0, 0, 30, 30))
      this._sortBgView.bgColor = 'rgba(0, 0, 0, 0.3)'
      this._sortBgView.cornerRadius = 15
    }
    return this._sortBgView
  }

  get scoreLabel () {
    if (!this._scoreLabel) {
      this._scoreLabel = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 30))
      this._scoreLabel.setFont(14, 'bold')
      this._scoreLabel.text = '0关'
      this._scoreLabel.textAlign = 'right'
      this._scoreLabel.textColor = '#a56f43'
    }
    return this._scoreLabel
  }

  setupUI() {
    this.bgColor = '#f9ce7a'
    this.cornerRadius = 5
    this.borderWidth = 2
    this.borderColor = '#9e441f'
    // this.addSubview(this.bgImageView)
    this.addSubview(this.sortImageView)

    this.sortBgView.anchorX = 0
    this.sortBgView.position = new ViewKit.Point(5, this.rect.height / 2)
    this.addSubview(this.sortBgView)

    this.sortLabel.text = '1'
    this.sortBgView.addSubview(this.sortLabel)

    this.avatarImageView.anchorX = 0
    this.avatarImageView.position = new ViewKit.Point(this.sortImageView.rect.right + 15, this.rect.height / 2)
    this.addSubview(this.avatarImageView)


    this.nameLabel.anchorX = 0
    this.nameLabel.position = new ViewKit.Point(this.avatarImageView.rect.right + 5, this.rect.height / 2)
    this.addSubview(this.nameLabel)

    this.scoreLabel.anchorX = 1
    this.scoreLabel.position = new ViewKit.Point(this.rect.right - 10, this.rect.height / 2)
    this.addSubview(this.scoreLabel)
  }

  set dataModel(value) {
      this._dataModel = value
      this.updateView(value)
  }

  get dataModel() {
      return this._dataModel
  }

  updateView(model) {
    if (model == null) {
      return
    }
    console.log(model);
    this.nameLabel.text = model.nickname

    let score = 0
    if (model.KVDataList.length != 0) {
      score = model.KVDataList[0].value
    }

    this.scoreLabel.text = '赢' + score + '场'
    this.avatarImageView.imageSrc = model.avatarUrl
    // this.avatarImageView.image.src = "https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erfJg8Y6J7dicTNgXSAPdqDVcicIjibIr1dbJLJmTdhcg2QcXjZiaUbBvVvW0uauib5w28gT9ULcDYB0MQ/132"

    if (model.sort < 3) {
      this.sortBgView.isHidden = true
      this.sortImageView.isHidden = false
      this.sortImageView.imageSrc = ViewKit.getRes('board_' + model.sort, 'js/openDataContext/images/')
    } else {
      this.sortImageView.isHidden = true
      this.sortBgView.isHidden = false
      this.sortLabel.text = (model.sort + 1).toString()
    }
  }

}
