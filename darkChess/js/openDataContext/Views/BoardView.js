import * as ViewKit from '../libs/ViewKit/index'
import BoardItemView from './BoardItemView'
import DataBus from '../DataBus'

export default class BoardView extends ViewKit.View {

  get selfSortView () {
    if (this._selfSortView == null) {
      let v = null
      v = new BoardItemView(ViewKit.Rect.rect(0, 0, this.boardBgView.width - 14, 50))
      v.position = new ViewKit.Point(this.boardFooter.width / 2, this.boardFooter.height / 2)
      v.bgColor = null
      this._selfSortView = v
    }
    return this._selfSortView
  }

  get titleLabel () {
    if (this._titleLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 30))
      v.textColor = 'white'
      v.text = '排行榜'
      v.textAlign = 'center'
      v.position = new ViewKit.Point(this.boardHeader.width / 2, this.boardHeader.height / 2)
      this._titleLabel = v
    }
    return this._titleLabel
  }

  get boardHeader () {
    if (this._boardHeader == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.width - 60, 50))
      v.bgColor = '#15a2fa'
      v.cornerRadiusArr = [15, 15, 0, 0]
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, 70)
      this._boardHeader = v
    }
    return this._boardHeader
  }

  get boardFooter () {
    if (this._boardFooter == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.boardHeader.width, 50))
      v.bgColor = '#aedfff'
      v.cornerRadiusArr = [0 ,0 , 15, 15]
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height - 125 - 15)
      this._boardFooter = v
    }
    return this._boardFooter
  }

  get boardBgView () {
    if (this._boardBgView == null) {
      let v = null
      let halfHeight = (this.height / 2) - (42 + 26 + 10)
      v = new ViewKit.ScrollView(ViewKit.Rect.rect(0, 0, 425, halfHeight * 2))
      // v.bgColor = 'black'
      v.position = this.center
      this._boardBgView = v
    }
    return this._boardBgView
  }

  setupUI() {

    this.sorts = []
    let views = [this.boardBgView]

    // this.boardHeader.addSubview(this.titleLabel)

    views.map(x => {
      this.addSubview(x)
    })

    // this.boardFooter.addSubview(this.selfSortView)

    // DataBus.share().getFirendScore().then(data => {
    //   this.dataModel = data
    // })
  }

  get dataModel () {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  refresh(){
    this.update(this.dataModel)
  }

  update(data) {
    if (!data) {
      return
    }
    this.sorts.map(x => x.removeFromParent())
    let baseTop = 12
    let gap = 8
    let last = null
    let tmp = data.concat()
    if (tmp.length > 20) {
      tmp.length = 20
    }
    tmp.map((x, index) => {
      let v = new BoardItemView(ViewKit.Rect.rect(0, 0, this.boardBgView.width - 28, 50))
      v.dataModel = x
      v.anchorY = 0
      v.position = new ViewKit.Point(this.boardBgView.width / 2, (50 + gap) * index + baseTop)
      this.sorts.push(v)
      this.boardBgView.contentView.addSubview(v)
      last = v
    })
    if (last && last.rect.bottom > this.boardBgView.height) {
      this.boardBgView.contentView.height = last.rect.bottom
    }

    //self
    if (DataBus.share().openId == null) {
      return
    }
    let result = this.dataModel.filter(x => {
      return x.openid == DataBus.share().openId
    })
    console.log(result);
    this.selfSortView.dataModel = result[0]
  }
}
