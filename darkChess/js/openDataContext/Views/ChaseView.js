import * as ViewKit from '../libs/ViewKit/index'
import DataBus from '../DataBus'

export default class ChaseView extends ViewKit.View {

  get label1 () {
    if (this._label1 == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 10))
      v.textColor = '#75c172'
      v.text = '还有1关'
      v.setFont(11, 'bold')
      v.anchorY = 1
      v.anchorX = 0
      v.position = new ViewKit.Point(5, this.bgView.height / 2 - 2)
      this._label1 = v
    }
    return this._label1
  }

  get label2 () {
    if (this._label2 == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 10))
      v.textColor = '#75c172'
      v.text = '超越他!'
      v.anchorY = 0
      v.anchorX = 0
      v.setFont(11, 'bold')
      v.position = new ViewKit.Point(5, this.bgView.height / 2 + 2)
      this._label2 = v
    }
    return this._label2
  }

  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 85, 92, 40))
      v.bgColor = '#e5e5e5'
      v.cornerRadiusArr = [0, 20, 20, 0]
      this._bgView = v
    }
    return this._bgView
  }

  get avatar () {
    if (this._avatar == null) {
      let v = null
      v = new ViewKit.ImageView("https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erKGTHNaqbsOPqVjQelVIouRSA7kwHl8A298x0a2pVMNYCXRIZ7ic3EWwkrdusg9nbGQbqbNeJNRYQ/132", ViewKit.Rect.rect(0, 0, 36, 36))
      v.cornerRadius = 18
      v.maskToBounds = true
      v.position = new ViewKit.Point(this.bgView.width - 20, this.bgView.height / 2)
      this._avatar = v
    }
    return this._avatar
  }

  setupUI() {
    this.addSubview(this.bgView)
    this.bgView.addSubview(this.avatar)
    this.bgView.addSubview(this.label1)
    this.bgView.addSubview(this.label2)
  }

  get dataModel () {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  update(model) {
    if (model == null) {
      this.bgView.isHidden = true
    } else {
      this.bgView.isHidden = false
      this.avatar.imageSrc = model.userInfo.avatarUrl
      this.label1.text = '还有' + model.offset + '关'
      this.label1.position = new ViewKit.Point(5, this.bgView.height / 2 - 2)
    }
  }
}
