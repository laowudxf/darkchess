import {Rect, Point, Size} from '../util'
import Device from '../Helper/Device'
import View from '../Views/View'

export default class ViewController {
  constructor() {
    this.screenRect = Rect.rect(0, 0, Device.share().screenWidth, Device.share().screenHeight)
    console.log(this.screenRect);
    this.viewControllers = []
    this.parentContainer = null
  }

  get vcIndex () {
    return this._vcIndex
  }

  set vcIndex(value) {
    this._vcIndex = value
  }


  loadView() {
    this.view = new View(Rect.rect(0, 0, Device.share().screenWidth, Device.share().screenHeight))
  }

  get view () {
    if (this._view == null) {
      this.loadView()
      this.viewDidLoad()
    }
    return this._view
  }

  set view(value) {
    this._view = value
  }

  viewDidLoad() {
    this.setupUI()
    this.addActionTarget()
  }

  setupUI() {}
  addActionTarget() {}

  viewWillAppear() {
    this.viewControllers.map(x => x.viewWillAppear())
  }

  viewWillDisappear() {
    this.viewControllers.map(x => x.viewWillDisappear())
  }

  viewDidAppear() {
    this.viewControllers.map(x => x.viewDidAppear())
  }

  viewDidDisappear() {
    this.viewControllers.map(x => x.viewDidDisappear())
  }


  //to do
  addChild(viewController) {
    if (viewController == null) {
      return
    }

    // if (this.currentViewController) {
    //   this.currentViewController.viewWillDisappear()
    //   this.currentViewController.view.removeFromParent()
    // }
    this.willAddChildViewController(viewController)

    viewController.viewWillAppear()
    this.viewControllers.push(viewController)
    viewController.parentContainer = this
    this.view.addSubview(viewController.view)
    viewController.viewDidAppear()
    this.didAddChildViewController(viewController)

    // if (this.currentViewController) {
    //   this.currentViewController.viewDidDisappear()
    // }

    // viewController.viewDidDisappear()
  }

  removeChild(viewController) {
    if (viewController == null) {
      return
    }

    let index = this.viewControllers.indexOf(viewController)
    if (index == - 1) {
      return
    }


    if (index >= 0) {
      this.willRemoveChildViewController(viewController)
      viewController.viewWillDisappear()
      // if (isCurrentVC && index > 0) {
      //   (this.viewControllers[index - 1]).viewWillAppear()
      // }
      viewController.parentContainer = null
      this.viewControllers.splice(index, 1)
      viewController.view.removeFromParent()
      viewController.viewDidDisappear()

      this.didRemoveChildViewController(viewController)
      // if (isCurrentVC && index > 0) {
      //   this.view.addSubview((this.viewControllers[index - 1]).view)
      //   let vc = this.viewControllers[index - 1]
      //   vc.viewDidAppear()
      // }
    }
  }

  willAddChildViewController() {}

  didAddChildViewController() {}

  willRemoveChildViewController() {}

  didRemoveChildViewController() {}

  removeFromParent() {
    if (this.parentContainer) {
      this.parentContainer.removeChild(this)
    }
  }
}
