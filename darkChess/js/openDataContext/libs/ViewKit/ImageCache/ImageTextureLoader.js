
import ImageLoader from './ImageLoader'
export default class ImageTextureLoader {
  static cacheWithJson(json) {
    let fileName = json.file
    let basePath = 'resource/'
    let imagePath = basePath + fileName
    return ImageLoader.share().cache(imagePath, fileName)
    .then(() => {
      let params = []
      for (let key in json.frames) {
        let info = json.frames[key]
        params.push([imagePath, key, {
          offset_x: info.x,
          offset_y: info.y,
          width: info.w,
          height: info.h,
          textureName: fileName
        }])
      }
      return ImageLoader.share().cacheArr(params)
    })
  }
}
