
let _deviceInstance
export default class Device {
  get scale() {
    return this.systemInfo.pixelRatio
  }

  get screenHeight () {
    return this.systemInfo.screenHeight
  }

  get screenWidth () {
    return this.systemInfo.screenWidth
  }

  get isPhoneX () {
    return this.systemInfo.model.indexOf('iPhone X') != -1
  }

  get isPhone () {
    return this.systemInfo.model.indexOf('iPhone') != -1
  }

  constructor() {
    if (_deviceInstance) {
      return _deviceInstance
    }
    this.systemInfo = wx.getSystemInfoSync()
    console.log('systemInfo:', this.systemInfo);
    _deviceInstance = this
  }

  static share() {
    return new Device()
  }
}
