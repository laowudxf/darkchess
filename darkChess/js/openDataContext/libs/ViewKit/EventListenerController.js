
let eventListenerInstance

export default class EventListenerController {

  static share() {
    return new EventListenerController()
  }

  constructor() {
    if (eventListenerInstance) {
      return eventListenerInstance
    }

    eventListenerInstance = this
    this.eventCallback = null

    console.log('catch event-----------------------');
    this.addListener()
  }

  addListener() {
    wx.onTouchStart((event) =>{
      event.type = 'touchstart'
      this.touchStart(event)
    })
  	wx.onTouchMove((event) => {
      event.type = 'touchmove'
        this.touchMove(event)
    })

  	wx.onTouchEnd((event) => {
      event.type = 'touchend'
        this.touchEnd(event)
    })

  	wx.onTouchCancel((event) => {
    })

  }

  // 唯一性 若确认 其他手势都变成fail
  setRecognitionGes(ges) {
    if (this.recognitionGes) {
      return
    }
    this.recognitionGes = ges
    let v = this.responsorView
    while (v != null) {
      v.gestures.filter(x => x != ges).map(x => {
        x.state = 6 // failed
      })
      v = null
    }
  }

  removeRecognitionGes() {
    this.recognitionGes = null
  }

  touchStart(e) {

    if (this.eventCallback) {
      this.responsorView = this.eventCallback(e)
      if (this.responsorView) {
        this.responsorView.touchEvent(e)
      }
    }

  }

  touchMove(e) {

    if (this.recognitionGes) {
      this.recognitionGes.touchEvent(e)
    } else {
      if (this.eventCallback) {
        if (this.responsorView) {
          this.responsorView.touchEvent(e)
        } else {
          let view = this.eventCallback(e)
          if (view) {
            view.touchEvent(e)
          }
        }
      }
    }
  }

  touchEnd(e) {

    if (this.recognitionGes) {
      this.recognitionGes.touchEvent(e)
    } else {
      if (this.responsorView) {
        this.responsorView.touchEvent(e)
      }

      if (this.eventCallback) {
        this.eventCallback(e)
      }
    }

  }
}
