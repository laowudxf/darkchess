import * as Util from '../util'
import EventListenerController from '../EventListenerController'
class GestureRecognzer {
  constructor(target, action) {
    this.target = target
    this.action = action
    this.startInThisView = false

    this.setup()
    /*
    state {
    UIGestureRecognizerStatePossible = 0,   // 默认的状态，这个时候的手势并没有具体的情形状态
    UIGestureRecognizerStateBegan,      // 手势开始被识别的状态
    UIGestureRecognizerStateChanged,    // 手势识别发生改变的状态
    UIGestureRecognizerStateEnded,      // 手势识别结束，将会执行触发的方法
    UIGestureRecognizerStateEnsure,      // 手势确认，将会执行触发的方法
    UIGestureRecognizerStateCancelled,  // 手势识别取消
    UIGestureRecognizerStateFailed,     // 识别失败，方法将不会被调用
  }
    */

    this.state = 0
  }

  get state() {
    return this._state
  }

  set state(value) {
    this._state = value
    if (value == 1) {
    } else if (value == 3) { // end
      // this.removeCurrentRecognitionGes()
    } else if (value == 2) {
    } else if (value == 4) { //ensure
      this.addToEventListener()
    } else if (value == 6) { //fail
      this.resetState()
    }
  }

  resetState() {
    this.startInThisView = false
    this.state = 0
    this.removeCurrentRecognitionGes()
  }


  setup() { }

  touchEvent(e) {
    if (e.type == 'touchend') {
      this.touches = []
      this.changedTouches = e.changedTouches
      this.firstTouchPoint =  new Util.Point(this.changedTouches[0].clientX, this.changedTouches[0].clientY)
      return
    }
    this.touches = e.touches
    this.firstTouchPoint =  new Util.Point(this.touches[0].clientX, this.touches[0].clientY)
  }

  addToEventListener () {
    EventListenerController.share().setRecognitionGes(this)
  }

  removeCurrentRecognitionGes() {
    EventListenerController.share().removeRecognitionGes()
  }
}

class PanGestureRecognzer extends GestureRecognzer {

  set state(value) {
    super.state = value
    if (value == 1) {
    } else if (value == 3) {
      if (this.hadMoved) {
        this.hadMoved = false
        this.action(this)
        this.resetState()
      }
    } else if (value == 2) {
      this.hadMoved = true
      this.action(this)
    }
  }

  get state() {
    return this._state
  }

  setup() {
      this.numberOfTapsRequired = 1
  }

  touchEvent(e) {
    super.touchEvent(e)
    this.targetRect = this.target.rectInView()
    switch (e.type) {
      case 'touchstart':
      if (this.targetRect.containPoint(this.firstTouchPoint)) {
        this.startInThisView = true
        this.startPoint = this.firstTouchPoint
        this.state = 1
      }
      break;
      case 'touchmove':
      if (this.startInThisView) {
        this.currentPoint = this.firstTouchPoint
        let offset = new Util.Point(this.currentPoint.x - this.startPoint.x ,this.currentPoint.y - this.startPoint.y)
        if (Math.abs(offset.x) <= 1 && Math.abs(offset.y) <= 1) {
          return
        }
        if (this.state == 1) {
          this.state = 4
          this.state = 2
        } else {
          this.state = 2
        }

      }
      break;
      case 'touchend':
      this.state = 3
      break;
      default:

    }
  }
}

class TapGestureRecognzer extends GestureRecognzer {

  set state(value) {
    super.state = value
    if (value == 1) {
    } else if (value == 3) {
      if (EventListenerController.share().recognitionGes == this) {
        this.action(this)
      }
      this.resetState()
    } else if (value == 2) {
    }
  }

  get state() {
    return this._state
  }

  setup() {
    this.numberOfTapsRequired = 1
  }

  touchEvent(e) {
    super.touchEvent(e)
    this.targetRect = this.target.rectInView()
    switch (e.type) {
      case 'touchstart':
      if (this.targetRect.containPoint(this.firstTouchPoint)) {
        this.startInThisView = true
        this.state = 1
        this.target.tapState = 1
      }
      break;
      case 'touchmove':
        this.target.tapState = this.targetRect.containPoint(this.firstTouchPoint) ? 1 : 0
      break;
      case 'touchend':
      if (this.targetRect.containPoint(this.firstTouchPoint) && this.startInThisView) {
        this.startPoint = this.firstTouchPoint
        this.state = 4
        this.state = 2
        this.state = 3
      }
      this.target.tapState = 0
      break;
      default:

    }
  }

}

export {GestureRecognzer, TapGestureRecognzer, PanGestureRecognzer}
