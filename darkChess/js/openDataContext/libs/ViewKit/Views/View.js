/**
 * 游戏基础的精灵类
 */
import {Rect, Point, Size} from '../util'
import Device from '../Helper/Device'

export default class View {
  constructor(rect = new Rect()) {
    this.name = 'View'

    //animations
    this.animatinoActions = []
    this.toActions = []
    this.animations = []
    // --------

    this.subViews = []
    this.subCanvas = []
    this.rect = Rect.rect(rect.x, rect.y ,rect.width, rect.height)
    this.isHidden = false
    this.bgColor = null
    this.parentView = null

    this.cornerRadius = 0
    this.cornerRadiusArr = []

    this.anchorX = 0.5
    this.anchorY = 0.5
    this.translation = new Point(0, 0)
    this.scale = new Point(1, 1)
    this.rotate = 0
    this.animationAnchorX = 0.5
    this.animationAnchorY = 0.5
    this.alpha = 1
    this.borderWidth = 0
    this.borderColor = null

    this.orientation = 0


    // ------- maskToBounds
    this.maskToBounds = false
    // ---------end maskToBounds

    //------- interaction
    this.userInteraction = true
    this.gestures = []
    this.tapState = 0 // 0 => normal 1 => tap select
    this.tapStateBlock = null
    //------- end
    this.inlineSetup()
    this.callLifiCycle()
  }

  // 0 => portrait, 1=> landscape
  get orientation () {
    return this._orientation
  }

  set orientation(value) {
    this._orientation = value
    if (value == 0) {
      this.rotate = 0
    } else if (value == 1) {
      this.rotate = Math.PI / 2
    }
  }

  get tapState () {
    return this._tapState
  }

  set tapState(value) {
    if (value == this._tapState) {
      return
    }
    this._tapState = value
    if (this.tapStateBlock) {
      this.tapStateBlock(value)
    }
  }

  //layout
  get center() {
    return new Point(this.x + this.width / 2, this.y + this.height / 2)
  }

  get x() {
    return this.rect.x
  }

  set x(value) {
      this.rect.x = value
  }

  get y() {
    return this.rect.y
  }

  set y(value) {
      this.rect.y = value
  }

  get width() {
    return this.rect.width
  }

  set width(value) {
      this.rect.width = value
  }

  get height() {
    return this.rect.height
  }

  set height(value) {
      this.rect.height = value
  }

  get bounds() {
    return Rect.rect(0, 0, this.rect.width, this.rect.height)
  }

  get position() {
    return new Point(this.rect.x + this.anchorX * this.rect.width, this.rect.y + this.anchorY * this.rect.height)
  }

  set position(value) {
    this.rect.x = value.x - this.anchorX * this.rect.width
    this.rect.y = value.y - this.anchorY * this.rect.height
  }

  get anchorX() {
    return this._anchorX
  }

  set anchorX(value) {
    this._anchorX = value
  }

  get anchorY() {
    return this._anchorY
  }

  set anchorY(value) {
    this._anchorY = value
    this.position = this.position
  }

  get translation() {
    return this._translation
  }

  set translation(value) {
    this._translation = value
  }

  get scale() {
    return this._scale
  }

  set scale(value) {
    this._scale = value
  }

  // ----------------- end layout

  get parentView () {
    return this._parentView
  }

  set parentView(value) {
    this._parentView = value
  }

  viewIsHidden() {
      var isHidden = this.isHidden
      var v = this
      while(v.parentView && isHidden == false) {
        v = v.parentView
        isHidden = v.isHidden || isHidden
      }

      return isHidden || this.drawRect == null
  }

  callLifiCycle() {
    this.setupUI()
    this.addActionTarget()
  }

  inlineSetup(){}
  setupUI(){}
  addActionTarget(){}

  addSubCanvas(canvas) {
    this.subCanvas.push(canvas)
  }

  removeSubCanvas(canvas) {
    let index = this.subCanvas.indexOf(canvas)
    if (index == -1){
      return
    }
    this.subCanvas.splice(index, 1)
  }

  addSubview(view) {
    // this.subViews.push(view)
    // view.parentView = this
    this.insertSubView(view, this.subViews.length)
  }

  insertSubView(view, index) {
    this.subViews.splice(index, 0, view)
    view.willMoveToSuperview(this)
    view.parentView = this
    view.didMoveToSuperview()
  }

  willMoveToSuperview(view){}
  didMoveToSuperview(){}

  addSubviewAbove(view, referView) {
    let index = this.indexOfSubView(referView)
    if (!index) {
      return
    }

    this.insertSubView(view, index + 1)
  }

  addSubviewBelow(view, referView) {
    let index = this.indexOfSubView(referView)
    if (!index) {
      return
    }

    this.insertSubView(view, index)
  }

  indexOfSubView(view) {
    let index = this.subViews.indexOf(view)
    return index >= 0 ? index: null
  }

  removeFromParent() {
    this.willMoveToSuperview(null)
    let index = this.parentView.subViews.indexOf(this)
    if (index >= 0) {
      this.parentView.subViews.splice(index, 1)
    }
    this.didMoveToSuperview()
  }

  draw(ctx, rect) {
    if (this.shouldBeDraw() == false) {
      return
    }

    if (this.bgColor == null) {
      return
    }

    ctx.fillStyle = this.bgColor

    let drawRectArr = this.clacDrawRect(rect)
    if (this.cornerRadius == 0 && this.cornerRadiusArr.length == 0 || this.maskToBounds == true) {
      ctx.fillRect(...drawRectArr)
      // ctx.strokeStyle = this.bgColor
      // ctx.lineWidth = 3
      // ctx.strokeRect(...drawRectArr)
    } else {
      this.drawRoundRect(ctx, rect)
      ctx.fill()
      // ctx.strokeStyle = this.bgColor
      // ctx.lineWidth = 3
      // ctx.stroke()
    }
    if (this.borderWidth > 0) {
      ctx.strokeStyle = this.borderColor
      ctx.lineWidth = this.borderWidth * Device.share().scale
      if (this.cornerRadius == 0 && this.cornerRadiusArr.length == 0 || this.maskToBounds == true) {
        ctx.strokeRect(...drawRectArr)
      } else {
        ctx.stroke()
      }
    }

  }

  drawRoundRect(ctx, rect) {
    let drawRectArr = this.clacDrawRect(rect)
      let drawRect = Rect.rect(...drawRectArr)

      if (this.cornerRadiusArr.length < 4) {
        while (this.cornerRadiusArr.length < 4) {
          this.cornerRadiusArr.push(this.cornerRadius)
        }
      }

      let pi = Math.PI
      ctx.lineWidth = 2;
      ctx.strokeStyle = "#F00";
      ctx.beginPath();

      let scale = Device.share().scale
      for (let index in this.cornerRadiusArr) {
        let i = parseInt(index)
        let r = this.cornerRadiusArr[i]
        r = scale * r
        if (i == 0) {
          ctx.moveTo(0 + drawRect.x, r + drawRect.y)
          ctx.arc(drawRect.x + r , drawRect.y + r, r,-pi, -pi / 2)
        } else if (i == 1) {
          ctx.arc(this.rect.width * scale - r + drawRect.x, r + drawRect.y, r, -pi + pi / 2, -pi / 2 + pi / 2)
        } else if (i == 2) {
          ctx.arc(this.rect.width * scale - r + drawRect.x, this.rect.height * scale - r + drawRect.y, r, -pi + (pi / 2) * 2, -pi / 2 + (pi / 2) * 2)
        } else if (i == 3) {
          ctx.arc( r + drawRect.x, this.rect.height * scale - r + drawRect.y, r, -pi + (pi / 2) * 3, -pi / 2 + (pi / 2) * 3)
        }
      }

      ctx.closePath()

  }

  calcDrawPoint(rect, point) {
    point.x += rect.x
    point.y += rect.y
    if (this.currentCtx.drawOffset) {
      point.x += this.currentCtx.drawOffset.x
      point.y += this.currentCtx.drawOffset.y
    }
    point.x *= Device.share().scale
    point.y *= Device.share().scale
    return point
  }

  clacDrawRect(rect) {
    let r = rect
    let tmp = Rect.rect(r.x, r.y, r.size.width, r.size.height)
    // if (this.currentCtx.drawOffset) {
    //   tmp.x += this.currentCtx.drawOffset.x
    //   tmp.y += this.currentCtx.drawOffset.y
    // }

    let scale = Device.share().scale
    // tmp.x += this.translation.x
    // tmp.y += this.translation.y
    tmp.x *= scale
    tmp.y *= scale
    tmp.width = tmp.width * scale
    tmp.height =  tmp.height * scale

    return tmp.rectArr()
  }

  extraDraw(ctx, rect) { }

  calcDrawRectAndCreateCanvas(parentRect, ctx) {
    this.drawRect = this.rect.rectInRect(parentRect)
    if (ctx) {
      this.currentCtx = ctx
    }
  }

  clipOp(ctx, rectArr) {
    ctx.beginPath()
    if ((this.cornerRadius == 0 && this.cornerRadiusArr.length == 0) == false) {
      this.drawRoundRect(ctx, this.drawRect)
    } else {
      ctx.rect(...rectArr)
    }
    ctx.clip()
  }


  shouldBeDraw() {
    let scale = Device.share().scale
    let r = !((this.drawRect.x * scale) > this.currentCtx.width || (this.drawRect.y * scale) > this.currentCtx.height)
    return r
  }

  removeAnimation() {
    this.animations.map(x => {
      x.cancel()
    })

    this.animations.length = 0
    this.toActions.length = 0
  }

  /**
  * 将精灵图绘制在canvas上
  */
  drawToCanvas(ctx, rect) {
    if (this.toActions.length > 0) {
      this.toActions[0].run(this)
    }

    this.calcDrawRectAndCreateCanvas(rect, ctx)


    if (this.isHidden || !ctx) {
      return
    }

    let tmpDrawRect = Rect.rect(this.drawRect.x, this.drawRect.y, this.drawRect.width, this.drawRect.height)

    if (this.shouldSaveContext) {
      let drawRectArr = this.clacDrawRect(this.drawRect)
      let r = Rect.rect(...drawRectArr)

      ctx.save()

      if (this.translation.x != 0 || this.translation.y != 0) {
        let scale = Device.share().scale
        ctx.translate(this.translation.x * scale, this.translation.y * scale)
      }

      this.rorateTmpOffset = new Size(r.x + r.width * this.animationAnchorX , r.y + r.height * this.animationAnchorY)
      if (this.rotate != 0) {
        ctx.translate(this.rorateTmpOffset.width, this.rorateTmpOffset.height)
        ctx.rotate(this.rotate)
        ctx.translate(-this.rorateTmpOffset.width, -this.rorateTmpOffset.height)
      }
      if (this.scale.x != 1 || this.scale.y != 1)  {
        ctx.translate(this.rorateTmpOffset.width, this.rorateTmpOffset.height)
        ctx.scale(this.scale.x, this.scale.y)
        ctx.translate(-this.rorateTmpOffset.width, -this.rorateTmpOffset.height)
      }

      if (this.maskToBounds) {
        this.clipOp(this.currentCtx, drawRectArr)
      }

      if (this.alpha != 1) {
        ctx.globalAlpha = this.alpha
      }
    }


    this.draw(this.currentCtx, tmpDrawRect)
    this.subViews.map(x => x.drawToCanvas(this.currentCtx, tmpDrawRect))
    this.drawSubCanvas(this.currentCtx, tmpDrawRect)

    this.extraDraw(ctx, tmpDrawRect)

    if (this.shouldSaveContext) {
      ctx.restore()
    }
  }

  ctxAddDrawOffset(ctx, offset) {
    if (ctx.drawOffset == null) {
      ctx.drawOffset = new Point(0, 0)
    }
    ctx.drawOffset.x += offset.x
    ctx.drawOffset.y += offset.y
  }

  ctxRemoveDrawOffset(ctx, offset) {
    if (ctx.drawOffset == null) {
      ctx.drawOffset = new Point(0, 0)
    }
    ctx.drawOffset.x -= offset.x
    ctx.drawOffset.y -= offset.y
  }

  drawSubCanvas(ctx, rect) {
    if (this.subCanvas.length == 0) {
      return
    }
    let drawRectArr = this.clacDrawRect(rect)
    this.subCanvas.map(x => ctx.drawImage(x, ...drawRectArr))
  }

  get shouldSaveContext() {
    return this.rotate != 0 || (this.scale.x != 1 || this.scale.y != 1) || this.alpha != 1 || this.maskToBounds || this.translation.x != 0 || this.translation.y != 0
  }


  //钩子函数
  touchEvent(event) {
    this.gestures.map(x => {
      x.touchEvent(event)
    })

    if (this.parentView) {
      this.parentView.touchEvent(event)
    }
  }

  //touch event
  // return View
  testHit(event) {
    if (this.viewIsHidden() || this.userInteraction == false) {
      return null
    }

    let touch = event.touches[0]
    if (!touch) {
      return null
    }
    // console.log(touch);
    let point = new Point(touch.clientX, touch.clientY)
    let rect = this.rectInView()
    if (rect.containPoint(point) == false) {
      return null
    }
    return this
  }

  findResponser(event) {
    if (this.subViews.length == 0) {
      return this
    }

    let r = this.subViews.filter((x) => {
      return x.testHit(event)
    })

    if (r.length == 0) {
      return this
    }

    let v = r[r.length - 1]
    return v.findResponser(event)

  }

  rectInView(view) {
    // let rect = this.drawRect
    // return rect

    let indexView = this
    let rect = this.rect

    while(indexView.parentView != null) {
      if (view && indexView == view) {
        return rect
      }

      rect = rect.rectInRect(indexView.parentView.rect)
      // rect = rect.rectInRect(tempRect)
      indexView = indexView.parentView
    }
    return rect
  }

  addGesture(value) {
    this.gestures.push(value)
  }

  pointInThisView(point) {
    let p = new Point(point.x, point.y)
    let rect = this.rectInView()
    p.x -= rect.x
    p.y -= rect.y
    return p
  }

  justCalcDrawRect(draw = false, aWindow) {
    let window = aWindow

    if (this.parentView == null && this != window) {
      return null
    }

    let r = null
    if (this.parentView == window || this.parentView == null) {
      r = this.rect
    } else {
      r = this.rect.rectInRect(this.parentView.justCalcDrawRect(false, window))
    }
    let tmp = Rect.rect(r.x , r.y, r.width, r.height)

    if (draw) {
      let scale = Device.share().scale
      tmp.x *= scale
      tmp.y *= scale
      tmp.width = tmp.width * scale
      tmp.height =  tmp.height * scale

    }
    return tmp
  }

  sendTapAction() {
    let ges = this.gestures[0]
    console.log(ges);
      ges.action(ges)
  }
}
