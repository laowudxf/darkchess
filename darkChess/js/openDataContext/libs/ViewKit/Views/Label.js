import View from './View'
import {screenScale, Rect} from '../util'
import Device from '../Helper/Device'

export default class Label extends View {
  setupUI(){
    this.setFont()
    this.name = "Label"
    this.textAlign = 'left'
    this.baseline = 'middle'
    this.text = 'label'
    this.bold = false
    this.textColor = '#ffffff'
    this.bgColor = null
    this.lineWidth = 0
    this.textBorderColor = 'red'
  }

  get text() {
    return this._text
  }

  set text(value) {
    return this._text = value
  }

  setFit() {
    this.shouldSetFit = true
  }

  inlineSetFit() {
    let metrics = this.currentCtx.measureText(this.text)
    this.rect.width = metrics.width / Device.share().scale
    this.rect.height = this._size
    this.position = this.lastPosition
  }

  setFont(size = 20, bold = 'normal' ,family = 'Microsoft Yahei') {
    this._size = size
    let fontStr =  bold + ' ' + size * Device.share().scale + 'px ' + family
    this.font = fontStr

  }

  draw(ctx, rect) {
    super.draw(ctx, rect)
    if (this.text == '') {
      return
    }


    ctx.textAlign = this.textAlign
    ctx.textBaseline = this.baseline
    ctx.fillStyle = this.textColor
    ctx.font = this.font
    if (this.lineWidth > 0) {
      ctx.lineWidth = this.lineWidth * Device.share().scale
      ctx.strokeStyle = this.textBorderColor
    }

    if (this.shouldSetFit) {
      this.shouldSetFit = false
      this.lastPosition = this.position
      this.inlineSetFit()
    }

    let rArr = this.clacDrawRect(rect)
    let r = Rect.rect(...rArr)
    if (this.textAlign == 'center') {
      if (this.lineWidth > 0) {
        ctx.strokeText(this.text, r.x + r.width / 2, r.y + r.height / 2)
      }
      ctx.fillText(this.text, r.x + r.width / 2, r.y + r.height / 2)
    } else if (this.textAlign == 'left') {
      if (this.lineWidth > 0) {
        ctx.strokeText(this.text, r.x, r.y + r.height / 2)
      }
      ctx.fillText(this.text, r.x, r.y + r.height / 2)
    } else if (this.textAlign == 'right') {
      if (this.lineWidth > 0) {
        ctx.strokeText(this.text, r.right, r.y + r.height / 2)
      }
      ctx.fillText(this.text, r.right, r.y + r.height / 2)
    }
  }


}
