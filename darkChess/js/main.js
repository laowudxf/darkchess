import './Extension/WxObjectExtension'
import * as ViewKit from './libs/ViewKit/index'
import DataBus from './DataBus'
import HomeViewController from './Views/VC/HomeViewController'
import DarkChessGameViewController from './Views/VC/DarkChessGameViewController'
import SocketHelper from './Helper/SocketHelper'
import Network from './Helper/Network'

import darkChess_json from '../resource/darkChess'


export default class Main {
  constructor() {

    // setTimeout(() => {
    this.app = new ViewKit.App({canvas, orientation: 1})
    // this.app = new ViewKit.App({canvas})
    if (DataBus.share().isDebug) {
      Network.config.baseUrl = 'http://caige-app.dev.max06.com/'
    }
    console.log(Network.config.baseUrl);

    this.cacheImage()
    .then(() => {
      this.setup()
      this.setupUI()
      DataBus.share().music_bg_open = true
      // ViewKit.SoundHelper.playSoundWithName('music_bg', null, true)
    })

    // SocketHelper.share().connect()

  // }, 2000)
  }

  cacheImage() {
    return  ViewKit.ImageTextureLoader.cacheWithJson(darkChess_json)
  }

  setup() {

    wx.showShareMenu({
      withShareTicket: true
    })

    wx.onShow( data => {
      console.log('onShow:', data);
      DataBus.share().shareTicket = data.shareTicket
      DataBus.share().onShowBlocks.map(x => {
        x()
      })

      DataBus.share().onShowBlocks.length = 0

    })

    wx.onHide( data => {
       console.log('onHide', data);
      DataBus.share().onHideBlocks.map(x => {
        x()
      })
      DataBus.share().onHideBlocks.length = 0
    })

    ViewKit.Button.soundName = 'button'
  }

  setupUI() {
    sharedCanvas.width = canvas.width
    sharedCanvas.height = canvas.height
    DataBus.postMessage({method: 'shouldReDraw'})

    ViewKit.Button.soundName = null
    let homeVC = new HomeViewController()
    let nav = new ViewKit.NavigationController(homeVC)
    this.app.window.rootViewController = nav
  }
}
