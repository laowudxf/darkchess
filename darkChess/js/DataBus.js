import * as config from './config'
import * as ViewKit from './libs/ViewKit/index'
import Network from './Helper/Network'
import EndPoint from './Helper/EndPoint'
import User from './DataModel/User'

export default class DataBus extends ViewKit.Observable {

  static share() {
    return new DataBus()
  }

  static postMessage(data) {
    const openDataContext = wx.getOpenDataContext();
    openDataContext.postMessage(data);
  }

  get music_bg_open () {
    return this._music_bg_open
  }

  set music_bg_open(value) {
    this._music_bg_open = value
    this.notice(value, 'music_bg_open')
    if (value) {
      ViewKit.SoundHelper.playSoundWithName('music_bg', null, true)
    } else {
      ViewKit.SoundHelper.stopSound('music_bg')
    }
  }

  get openVoice () {
    return this._openVoice
  }

  set openVoice(value) {
    this._openVoice = value
    this.notice(value, 'openVoice')
    ViewKit.SoundHelper.openVoice = value
  }

  get preProduct() {
    let build = this.server_config.build
    if (build != null) {
      if (this.config.build >= build && this.server_config.preproduct) {
        return true
      }
    }
    return false
  }


  constructor() {
    super()
    if (DataBus.shareInstance) {
      return DataBus.shareInstance
    }

    DataBus.shareInstance = this
    this.config = config.config
    this.openVoice = true
    this.passData = null
    // this.isDebug = true
    this.isDebug = false

    //callbacks
    this.onShowBlocks = []
    this.onHideBlocks = []
  }

  serverLogin() {
    let wxUserInfo = User.share().wx_userInfo
    return EndPoint.login(wxUserInfo.nickName, wxUserInfo.avatarUrl)
    .then((data) => {
      console.log('userinfo:', data.data);
      User.share().userInfo = data.data.data
      this.userInfo = User.share().userInfo
      DataBus.postMessage({
        method: 'setOpenId',
        param: {openId: this.userInfo.openid}
      })
      return data.data.data
    })
    .catch(err => {
      console.log(err);
    })
  }

  //openDataView
  showRankBoard() {
    DataBus.postMessage({
      method: 'setRootView',
      param: {
        index: 0
      }
    })

    this.showOpenDataView()
  }

  hiddenBoard() {
    this.showOpenDataView(false)
  }

  showOpenDataView(show = true) {
    if (show) {
      DataBus.postMessage({
        method: 'shouldRefresh',
        param: true
      })
      ViewKit.App.share().window.addSubCanvas(sharedCanvas)

    } else {
      DataBus.postMessage({
        method: 'shouldRefresh',
        param: false
      })
      ViewKit.App.share().window.removeSubCanvas(sharedCanvas)

    }
  }
}
