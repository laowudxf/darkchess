import Network from './Network'
const HashSha1 = require('./sha1.js');
import User from '../DataModel/User'

class EndPoint {
  //uri
  static serverlogin(data = {}, method = 'GET') {
    return Network.request({
      url: 'wx/onLogin',
      data,
      method
    })
  }

  // action

  static login(name, icon) {
      return wx.ex_login().then((loginResp) => {
          return this.serverlogin({
            code: loginResp.code,
            nickname: name,
            icon: icon
          })
      })
  }

  static pass(data = {}, method = 'GET') {
    return Network.request({
      url: 'pass',
      data,
      method
    }, 'stroke')
  }

  static config(data = {}, method = 'GET') {
    return Network.request({
      url: 'config',
      data,
      method
    })
  }


  static modifyUserInfo(type, value, method = 'POST') {
    let userInfo = User.share().userInfo
    let data = {
        x_token: userInfo.x_token,
        type: type,
        value: '' + value,
        time: Date.now(),
        key: HashSha1(userInfo.id, Date.now() + type + value)
      }
    return Network.request({
      url: 'user/value',
      data,
      method
    })

  }

  static userInfo() {
    let userInfo = User.share().userInfo
    return Network.request({
      url: ('user/' + userInfo.id),
      data: {x_token: userInfo.x_token},
      method: 'GET'
    })
  }

}

EndPoint.BaseUrl = ''

export default EndPoint
