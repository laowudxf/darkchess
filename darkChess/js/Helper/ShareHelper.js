import * as ViewKit from '../libs/ViewKit/index'
import DataBus from '../DataBus'
import User from '../DataModel/User'

export default class ShareHelper {
  static share(validTime) {

    // wx.aldSendEvent("分享")

    let list = [
      ['年轻人都爱玩？越玩越年轻！', '01'],
      ['全部通关了！10分钟你能过几关！', '02'],
      // ['嘘！别被人发现，最好三更半夜玩！', '03'],
      ['得跟上时代节奏，必须玩起来！', '04'],
      ['好像他们都在玩呀！有这么好玩吗？', '05'],
    ]
    let index = this.getRandomInt(list.length - 1)
    let info = list[index]

    console.log(info);

    if (validTime) {
      DataBus.share().shareTime = Date.now()
    }

    let p = new Promise((s, f) => {
      if (validTime) {
        DataBus.share().onShowBlocks.push(() => {
          let offset = (Date.now() - DataBus.share().shareTime)
          console.log('shareTime:', DataBus.share().shareTime);
          console.log('offset:', offset);
          console.log('validTime:', validTime);
          if (offset > (validTime * 1000)) {
            s()
          } else {
            f()
          }
        })
      } else {
        s()
      }
    })

    let title = info[0]
    if (index == 1) {
      title = '我' + title
    }
    wx.shareAppMessage({
      title: title,
      imageUrl: ViewKit.getRes('share/' + info[1], 'images/', 'jpg')
    })

    return p
  }

  static shareInfo() {

    let list = [
      ['年轻人都爱玩？越玩越年轻！', '01'],
      ['全部通关了！10分钟你能过几关！', '02'],
      // ['嘘！别被人发现，最好三更半夜玩！', '03'],
      ['得跟上时代节奏，必须玩起来！', '04'],
      ['好像他们都在玩呀！有这么好玩吗？', '05'],
    ]
    let index = this.getRandomInt(list.length - 1)
    let info = list[index]
    return {title: info[0], imageUrl:ViewKit.getRes('share/' + info[1], 'images/', 'jpg')}

  }

  static getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
}
