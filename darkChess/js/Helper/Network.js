class Network {

  static request(params, prefix = 'program') {
    params.url = this.config.baseUrl  + prefix + '/' + params.url
    params.data.app = this.config.appId
    return wx.ex_request(params)
  }

}

Network.config = {
    baseUrl: 'https://caige1.sanliwenhua.com/',
    appId: 79
}

export default Network
