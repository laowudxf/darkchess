
import * as ViewKit from '../libs/ViewKit/index'

export default class GameConnectBox extends ViewKit.Observable {
  constructor(socket) {
    super()
    // 0 -> 初始 1 ->进入页面 2 -> 准备完毕 3 -> 对方投降
    this.receiveHearts = []
    this.gameState = 0
    this.socket = socket
    this.isGameOver = false
    this.receivePieceAction = null
    this.receiveBoardAction = null
    this.socket.onMessageBlock = (data) => {
      console.log('recive:',data);
      let d = null
      try {
        d = JSON.parse(data.data)
        this.parseData(d)
      } catch(e) {
        console.log(e);
      }

    }
  }

  get gameState () {
    return this._gameState
  }

  set gameState(value) {
    this.notice(value, 'gameState')
    this._gameState = value
  }

  // get isOtherReady () {
  //   return this._isOtherReady
  // }
  //
  // set isOtherReady(value) {
  //   this.notice(value, 'isOtherReady')
  //   this._isOtherReady = value
  // }

  get isGameOver () {
    return this._isGameOver
  }

  set isGameOver(value) {
    this.notice(value, 'isGameOver')
    this._isGameOver = value
  }

  parseData(data) {
    let type = data.type
    switch (type) {
      case 'over': //游戏结束
      this.isGameOver = true
      break;
      case 'play': //游戏间交互
      this.gameExchange(data.attach, data)
      default:
      break;
    }
  }

  gameExchange(data, origin) {
    console.log(data);
    switch (data.command) {
      case 'ready':
      this.gameState = 1
      this.startSendHeartData()
      break;
      case 'sendboard':
      console.log(origin);
      this.boardInfo = origin.value
      this.gameState = 2
      break
      case 'receiveBoard' :
      this.gameState = 2
      break
      case 'piecesAction':
      if (this.receivePieceAction) {
        this.receivePieceAction(data.param)
      }
      break
      case 'boardAction':
      if (this.receiveBoardAction) {
        this.receiveBoardAction(data.param)
      }
      break
      case 'surrender':
      this.gameState = 3
      break
      case 'reqPeace':
      if (this.reqPeaceBlock) {
        this.reqPeaceBlock()
      }
      break
      case 'accpetPeace':
      if (data.param.isAccpet) {
        this.gameState = 4
      }
      case 'heart':
        this.receiveHearts.push(1)
      break
      // if (this.accpetPeace) {
      //   this.accpetPeace()
      // }
      break
      default:

    }
  }

  listingHeart() {
    this.listingHeartId = setInterval(() => {
      if (this.receiveHearts.length > 0) {
        this.receiveHearts.length = 0
      } else {
        if (this.gameState != 5) {
          this.gameState = 5
        }
      }
      }, 5000)
  }

  accpetPeace(isAccpet = true) {
      this.send('', {
        command: 'accpetPeace',
        param: {
          isAccpet
        }
      })
      if (isAccpet) {
        this.gameState = 4
      }
  }

  startSendHeartData() {
    this.heartDataId = setInterval(() => {
      console.log('send heart');
      this.send('', {
        command: 'heart',
        param: {
          time: Date.now()
        }
      })
    }, 2000)
  }

  stopSendHeartData() {
    if (this.heartDataId) {
      clearInterval(this.heartDataId)
      this.heartDataId = null
    }
    if (this.listingHeartId) {
      clearInterval(this.listingHeartId)
      this.listingHeartId = null
    }
  }

  sendPiecesAction(x, y) {
      this.send('', {
        command: 'piecesAction',
        param: {
          x,
          y
        }
      })
  }

  sendBoardAction(x, y) {
      this.send('', {
        command: 'boardAction',
        param: {
          x,
          y
        }
      })
  }

  overGame() {
    // {'x_token': x_token,'data':{'room':'55ROOM','value':55},'type':'over'}
    this.stopSendHeartData()
    this.socket.send({room: this.matchingData.room, value: 0}, 'over')

  }

  surrender() {
    this.stopSendHeartData()
    this.send('', {
      command: 'surrender'
    })
    this.socket.send({room: this.matchingData.room, value: 0}, 'surrender')
  }

  robot() {
    this.stopSendHeartData()
    this.send()
    this.socket.send({room: this.matchingData.room, value: 0}, 'robot')
  }

  reqPeace() {
    wx.showToast({
      title: '已成功发送求和',
    })

    this.send('', {
      command: 'reqPeace'
    })
  }

  receiveBoard() {
    this.send('', {
      command: 'receiveBoard'
    })
  }

  send(value, attach = {}) {
    this.socket.send({room: this.matchingData.room, value, attach}, 'play')
  }

  beReady() {
      this.send('', {
        command: 'ready',
        param: {}
      })
  }
}
