
import User from '../DataModel/User'

export default class SocketHelper {
  static share() {
    return new SocketHelper()
  }
  constructor() {
    if (SocketHelper.shareInstance) {
      return SocketHelper.shareInstance
    }
    SocketHelper.shareInstance = this

    this.onMessageBlock = null
    this.setup()
  }

  setup() {
    // this.url = 'ws://118.31.77.172:5200'
    this.url = 'wss://caige1.sanliwenhua.com:5200'
  }

  connect(success) {
    if (this.task) {
      this.task.close({
        complete: () => {
          this.task = null
          this.connect(success)
        }
      })
      return
    }

    this.task = wx.connectSocket({
      url: this.url,
      success: (data) => {
        console.log(data);
      }
    })

    this.task.onMessage((data) => {
      console.log('recive:',data);
      if (this.onMessageBlock) {
        this.onMessageBlock(data)
      }
    })

    this.task.onOpen(() => {
      if (success) {
        success()
      }
    })
  }

  finishGame(room) {
      // {'x_token': x_token,'data':{'room':'55ROOM','value':55},'type':'over'}
      this.send({room, value: 0}, 'victory')
  }

  send(data = '1', type = 'matching') {
    let param = {
      x_token: User.share().userInfo.x_token,
      data,
      type
    }
    // console.log(param);
    let json = JSON.stringify(param)
    // console.log('send: ', json);
    this.task.send({
      data: JSON.stringify(param)
    })
  }

  disConnect() {
    if (this.task == null) {
      return
    }

    this.task.close({
      success: () => {
        console.log('disConnect websocket');
      }
    })
  }

}
