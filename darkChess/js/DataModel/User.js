import * as ViewKit from '../libs/ViewKit/index'
import EndPoint from '../Helper/EndPoint'
import DataBus from '../DataBus'

export default class User extends ViewKit.Observable {
  constructor() {
    super()
    if (User.shareInstance) {
      return User.shareInstance
    }

    User.shareInstance = this
  }

  get coin () {
    return this._coin
  }

  set coin(value) {
    this.saveCoin(offset)
    this.notice(value, 'coin')
    this._coin = value
  }

  static share() {
    return new User()
  }

  get userInfo () {
    return this._userInfo
  }

  set userInfo(value) {
    this.notice(value, 'userInfo')
    this._userInfo = value
    // this.coin = 1000
    this.parseOption(value.values.option)
  }

  parseOption(option) {
    if (option == null) {
      return
    }

    try {
      let data = JSON.parse(option)
      this.victory = data.victory
      this.fail = data.fail
      this.peace = data.peace
    } catch (err) {
      console.log(err);
      return
    }
  }


  get victoryRatio() {
      let count = this.victory + this.fail + this. peace
      if (count == 0) {
        return 0
      }

      return (this.victory / count * 100).toFixed(2) + '%'
  }

  get victory () {
    return this._victory == null ? 0 : this._victory
  }

  set victory(value) {
    this._victory = value
    DataBus.postMessage({
      method: 'saveHighestScore',
      param: {
        score: value
      }
    })
    this.updateCombatGains()
  }

  get fail () {
    return this._fail == null ? 0 : this._fail
  }

  set fail(value) {
    this._fail = value
    this.updateCombatGains()
  }

  get peace () {
    return this._peace == null ? 0 : this._peace
  }

  set peace(value) {
    this._peace = value
    this.updateCombatGains()
  }

  saveCoin(addOrSubCoin) {
    return EndPoint.modifyUserInfo('gold', addOrSubCoin)
  }

  updateCombatGains() {
    let data = {
      victory: this.victory,
      fail: this.fail,
      peace: this.peace
    }

      EndPoint.modifyUserInfo('option', JSON.stringify(data))
  }

}
