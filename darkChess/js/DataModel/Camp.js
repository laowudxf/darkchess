
export default class Camp {
  constructor(id) {
    this.player = null
    this.id = id
    let colors = ['red', 'black']
    if (id > 1) {
      this.color = colors[1]
    } else {
      this.color = colors[id]
    }
  }

}
