
import ViewController from './ViewController'

export default class NavigationController extends ViewController {
  constructor(rootViewController) {
    super()
    this.addChild(rootViewController)
    this.vcIndex = 0
  }

  viewWillAppear() {
    this.currentViewController.viewWillAppear()
  }

  viewWillDisappear() {
    this.currentViewController.viewWillDisappear()
  }

  viewDidAppear() {
    // this.currentViewController.viewDidAppear()
  }

  viewDidDisappear() {
    this.currentViewController.viewDidDisappear()
  }

  get currentViewController() {
    if (this.viewControllers.length == 0 || this.vcIndex == null) {
      return null
    }

    if (this.vcIndex >= this.viewControllers.length) {
       return null
    }
    return this.viewControllers[this.vcIndex]
  }

  // viewDidLoad() {
  //   this.push(this.rootViewController)
  // }

  willAddChildViewController(viewController) {
    if (this.currentViewController) {
      this.currentViewController.viewWillDisappear()
      this.currentViewController.view.removeFromParent()
    }
  }

  didAddChildViewController(viewController) {
    if (this.currentViewController) {
      this.currentViewController.viewDidDisappear()
    }
  }

  willRemoveChildViewController(viewController) {
    let index = this.viewControllers.indexOf(viewController)
    let isCurrentVC = viewController == this.currentViewController
    if (isCurrentVC && index > 0) {
      (this.viewControllers[index - 1]).viewWillAppear()
    }
  }

  didRemoveChildViewController(viewController) {
    this.view.addSubview((this.viewControllers[this.vcIndex - 1]).view)
    let vc = this.viewControllers[this.vcIndex - 1]
    vc.viewDidAppear()
  }

  push(vc) {
    this.addChild(vc)
    this.vcIndex += 1
  }

  pop() {
    if (this.currentViewController) {
      this.currentViewController.removeFromParent()
      this.vcIndex -= 1
    }
  }

  popToRootViewController() {
    for (let i = (this.viewControllers.length - 1); i > 0; i--) {
      this.pop(this.viewControllers[i])
    }
  }

}
