
export default class ImageLoader {
  static share() {
    return new ImageLoader()
  }

  constructor() {
    if (ImageLoader.shareInstance) {
      return ImageLoader.shareInstance
    }

    ImageLoader.shareInstance = this

    this.imageCacheMap = {}

  }

  getCache(name) {
    return new Promise((s, f) => {
      let cache = this.imageCacheMap[name]
      if (cache == null) {
        f()
        return
      }
      s(cache)
    })
  }

  cache(imageSrc, name, offsetInfo = null) {
    return new Promise((s, f) => {

      if (this.hasCache(name)) {
        s()
        return
      }

      let image = null
      if (offsetInfo == null) {
        image =  wx.createImage();
        image.src = imageSrc
        image.onload = () => {
          s()
        }
        this.imageCacheMap[name] = {image, offsetInfo}
      } else {
        this.getCache(offsetInfo.textureName)
        .then((x) => {
          this.imageCacheMap[name] = {image: x.image, offsetInfo}
          s()
        })
      }

    })
  }

  hasCache(name) {
    return this.imageCacheMap[name] != null
  }

  cacheArr(imageSrcArr) {
    let promiseArr = imageSrcArr.map(x => {
      if (imageSrcArr.length == 2) {
        return this.cache(x[0], x[1])
      } else {
        return this.cache(x[0], x[1], x[2])
      }
    })

    return Promise.all(promiseArr)
  }

}
