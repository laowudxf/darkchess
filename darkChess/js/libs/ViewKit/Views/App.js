import * as Util from '../util'
import Device from '../Helper/Device'
import EventListenerController from '../EventListenerController'
import View from './View'
import Window from './Window'

let appInstance

export default class App {

  static share() {
    return new App()
  }

  get scale() {
    return this.systemInfo.pixelRatio
  }

  get screenHeight () {
    return this.systemInfo.screenHeight
  }

  get screenWidth () {
    return this.systemInfo.screenWidth
  }

  get rootView() {
    if (this.window.subViews.length > 0) {
      return this.window.subViews[0]
    } else {
      return null
    }
  }

  set rootView(value) {
    this.window.subViews.map((x) => x.removeFromParent())
    this.window.addSubview(value)
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
  }

  constructor(param) {
    if (appInstance) {
      return appInstance
    }
    this.shouldRefresh = true

    appInstance = this

    this.systemInfo = wx.getSystemInfoSync()
    this.canvas = param.canvas

    if (param.orientation == 1 && Device.share().isPhone) {
      let width = this.canvas.width
      let height = this.canvas.height
      this.canvas.width = height * Device.share().scale
      this.canvas.height = width * Device.share().scale

    } else {
      this.canvas.width = this.canvas.width * Device.share().scale
      this.canvas.height = this.canvas.height * Device.share().scale
    }

    this.width = Device.share().screenWidth
    this.height = Device.share().screenHeight
    this.rect = Util.Rect.rect(0, 0, this.width, this.height)



    this.setupView()
    this.setupCanvas()
    this.setupLoop()
    this.installEventListener()

    this.refreshView = this.window
    this.refreshTimeInterval = null
  }

  installEventListener() {
    EventListenerController.share().eventCallback = (e) => {
      if (e.type == 'touchend' || e.type == 'ontouchend') {
        return
      }
      let v = this.window.findResponser(e)
      return v
    }
  }

  setupView() {
    this.window = new Window()
  }

  setupCanvas() {
    this.ctx = this.canvas.getContext('2d')
  }

  setupLoop() {
    this.bindloop = this.runtime.bind(this)
    this.aniId = window.requestAnimationFrame(
      this.bindloop,
      this.canvas
    )
  }

  runtime() {
    this.loop()
    this.aniId = window.requestAnimationFrame(
      this.bindloop,
      this.canvas
    )
  }

  loop() {
    if (this.refreshTimeInterval) {
      if (this.flag == null) {
        this.flag = 0
      }
      this.flag += 1
      if (this.flag < this.refreshTimeInterval) {
        return
      }
      this.flag = 0
    }
    if (this.shouldRefresh == false) {
      return
    }
    // let rect = this.rect
    // this.ctx.beginPath()
    // this.ctx.save()
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.refreshView.drawToCanvas(this.ctx, this.refreshView.justCalcDrawRect(false, this.window))
    // this.ctx.restore()
  }

}
