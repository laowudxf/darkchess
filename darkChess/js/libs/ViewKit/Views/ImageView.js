
import View from './View'
import Label from './Label'
import {Rect, Point, Size, drawUsingArc} from '../util'
import EventListenerController from '../EventListenerController'
import Device from '../Helper/Device'
import ImageCache from '../Helper/ImageCache'
import ImageLoader from '../ImageCache/ImageLoader'
import {screenScale}  from '../util'

import WXApi  from '../Helper/WXApi'

export default class ImageView extends View {
  constructor(imgSrc = '', rect = Rect.zero()) {
    super(rect)
    this.name = 'imageView'
    if (imgSrc) {
      this.imageSrc = imgSrc
    }

    this.userInteraction = false
    this.updateSize = null

  }

  get imageName () {
    return this._imageName
  }

  set imageName(value) {
    this._imageName = value
    this.imageOffsetInfo = null
    ImageLoader.share().getCache(value)
    .then((imageMap) => {
      this.image = imageMap.image
      this.imageOffsetInfo = imageMap.offsetInfo
      this.imageLoaded = true
    })
  }

  set imageSrc(value) {
      this._imageSrc = value
      let httpIndex = value.indexOf('http')
      if (httpIndex == -1) { //local
        let index = value.lastIndexOf("\/");
        let str = value.substring(index + 1, value.length);
        index = str.indexOf('.')
        let name = str.substring(0, index)

        if (ImageLoader.share().hasCache(name)) {
          this.imageName = name
          return
        }
      }

      this.image = wx.createImage();
      this.imageLoaded = false
      if (value.indexOf('http://p9ofir1eb.bkt.clouddn.com') == 0) {
        value = value.replace('http://p9ofir1eb.bkt.clouddn.com', 'https://res22.sanliwenhua.com')
      }
      ImageCache.share().getCache(value)
      .then(x => {
        this.imageHasCached = x.cached
        this.image.src = x.url
      })
      .catch((err) => {
        console.log(err);
      })
      this.image.onload = () => {
        this.imageLoaded = true
        if (this.imageHasCached == false) {
          console.log('should cache');
          Promise.resolve(`${wx.env.USER_DATA_PATH}/${encodeURIComponent(value)}`).then(filePath => {
            WXApi.downloadFile({ url: value}).then(res => {
              return WXApi.FileManager.saveFile({ tempFilePath: res.tempFilePath, filePath })
            })
          })
        }
      }
  }

  get imageSrc() {
    return this._imageSrc
  }

  get image() {
    return this._image
  }

  set image(value) {
    this._image = value
    this.imageLoaded = false
  }



  setupUI() {
    this.imageMode = 'fill'// fill, aspect, aspectFill
  }

  draw(ctx, rect) {

    if (!this.imageLoaded) {
      return
    }

    let drawRectArr = this.clacDrawRect(rect)
    let r = Rect.rect(...drawRectArr)
    switch (this.imageMode) {
      case 'fill':
      // console.log('draw image');
      if (this.imageOffsetInfo != null) {
        ctx.drawImage(this.image, this.imageOffsetInfo.offset_x, this.imageOffsetInfo.offset_y, this.imageOffsetInfo.width, this.imageOffsetInfo.height,...drawRectArr)
      } else {
        ctx.drawImage(this.image, ...drawRectArr)
      }
      break;
      case 'aspect':
        break;
      case 'aspectFill':
        break;
      case 'repeat':
        let pattern = ctx.createPattern(this.image, 'repeat')
        ctx.fillStyle = pattern
        ctx.fillRect(...drawRectArr)
        break;
      default:
      ctx.drawImage(this.image, r.x, r.y, r.width, r.height)
    }
  }

  // --------- experiment
  setFit () {
    this.shouldBeFit = true
  }

}
