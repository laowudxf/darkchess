import './weapp-adapter'
import Button from "./Views/Button"
import ImageView from "./Views/ImageView"
import Label from "./Views/Label"
import ScrollView from "./Views/ScrollView"

import * as Util from "./util"
import {Rect, Point, Size, systemInfo, screenScale, getRandomInt} from "./util"
import EventListenerController from "./EventListenerController"
import App from "./Views/App"
import Animation from "./AnimationAction"
import {getRes} from "./Helper/Resource"
import {valueWithKeyPath, setValueWithKeyPath} from "./Helper/Helper"
import Observable from './Observable/Observable'
import Device from './Helper/Device'
import SoundHelper from './Helper/SoundHelper'
import Storage from './Helper/Storage'
import ImageCache from './Helper/ImageCache'
import WXApi from './Helper/WXApi'
import View from "./Views/View"
import ViewController from "./ViewController/ViewController"
import NavigationController from "./ViewController/NavigationController"
import ImageLoader from './ImageCache/ImageLoader'
import ImageTextureLoader from './ImageCache/ImageTextureLoader'

export {Button, ImageView, Label, ScrollView, Util, Rect, systemInfo, screenScale, Point, Size, EventListenerController, App, Animation, getRes, valueWithKeyPath, setValueWithKeyPath, Observable, Device, SoundHelper, View, Storage, WXApi, ImageCache,
  ViewController, NavigationController, ImageLoader, ImageTextureLoader}
export * from './GestureRecognzer/GestureRecognzer'
export * from './Observable/Observable'
