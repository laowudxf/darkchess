
export default class SoundHelper {
  static playSoundWithName(name, finish, loop = false) {
    if (this.cache == null) {
      this.cache = {}
    }

    if (this.openVoice == false) {
      if(finish) {
        finish()
      }
      return
    }

    let audio = this.cache[name]

    if (!audio) {
      audio = wx['createInnerAudioContext']()
      audio.src = 'audio/' + name + '.mp3' // src 可以设置 http(s) 的路径，本地文件路径或者代码包文件路径
      audio.loop = loop
      audio.onError(err => {
        console.log('sound err:', err);
      })
      this.cache[name] = audio
    }

    //cache
    if (audio.paused == false) {
      audio.stop()
    }
    audio.play()
    if (finish) {
      audio.onEnded(() => {
        finish()
      })
    }
  }

  static stopSound(name) {
    if (name) {
      let audio = this.cache[name]
      if (audio && audio.paused == false) {
        audio.stop()
      }

    } else {
      for (let key in this.cache) {
        let audio = this.cache[key]
        if (audio.paused == false) {
          this.cache[key].stop()
        }
      }
    }
  }

  static soundSwitch() {
    this.openVoice = !this.openVoice
    // if (this.openVoice == false) {
    //   this.stopSound()
    // }
  }
}

SoundHelper.openVoice = true
