let LoopGetApi = function(apis, seed, sync = [], filter = []) {
	Object.keys(apis).forEach(api => {
		if (seed[api] || filter.indexOf(api) != -1) return;
		if (api.indexOf('Sync') != -1 || sync.indexOf(api) != -1) {
			seed[api] = apis[api];
		} else {
			seed[api] = (args, key) => new Promise((success, fail) => apis[api](Object.assign({ success, fail }, key ? { [key]: args } : args)));
		}
	});
};

export default new class WXApi {
	FileManager = {};
	Req = { baseURL: '', baseData: {} };
	Storage = {
		Get(key) {
			return new Promise((success, fail) => wx.getStorage(Object.assign({ success, fail }, { key }))).then(r => r.data).catch(() => Promise.resolve());
		},
		Set(key, data) {
			return new Promise((success, fail) => wx.setStorage(Object.assign({ success, fail }, { key, data })));
		},
	};
	// static share() {
	// 	return new WXApi
	// }
	constructor() {
		// if (WXApi.share) {
		// 	return WXApi.share
		// }

		// WXApi.share = this
		LoopGetApi(wx, this, [
			'getFileSystemManager',
			'onMessage',
			'loadFont',
			'onShareAppMessage',
			'createRewardedVideoAd',
			'createBannerAd',
			'createGameClubButton',
			'aldSendEvent',
			'onShow',
			'openCustomerServiceConversation',
		]);
		if (wx.getFileSystemManager) {
			let FileManager = wx.getFileSystemManager();
			LoopGetApi(FileManager, this.FileManager);
		}
		if (this.request) {
			['post', 'get'].forEach(method => {
				this.Req[method] = (url, data, other) => {
					return this.request(Object.assign({ method, data: Object.assign({}, this.Req.baseData, data), url: this.Req.baseURL + url }, other)).then(
						res => {
							console.log(res);
							if (res.statusCode == 200) {
								if (res.data.code == 200) return res.data.data;
								return res.data;
							}
							return Promise.reject('接口请求失败');
						},
					);
				};
			});
		}
	}
}
