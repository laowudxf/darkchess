
//
// interface Observable {
//   observers: any
//   registerObserver: (observer: Observer, key: string) => void
//   removeObserver: (observer: Observer, key: string) => void
//   notice: (value: any, key: string) => void
// }
//
// interface Observer {
//     subscribe: (before: any, now: any, key: string,instance: Observable) => void
// }

export default class Observable {
  constructor() {
    this.observers = {}
  }

  removeObserver( observer, key) {
    if (key != null) {
      let lists = this.observers[key]
      if (lists == null) {
        return
      }

      let index = lists.indexOf(observer)
      if (index != null) {
        lists.splice(index, 1)
      }
    } else {
      for (let key in this.observers) {
        let lists = this.observers[key]
        if (lists == null) {
          continue
        }

        let index = lists.indexOf(observer)
        if (index != -1) {
          lists.splice(index, 1)
        }
      }
    }
  }

  registerObserver(observer, key) {
    let lists = this.observers[key]

    if (lists == null) {
      lists = []
      this.observers[key] = lists
    }

    if (lists.indexOf(observer) == -1) {
      lists.push(observer)
    }

    observer.subscribe(this[key], this[key], key, this)
  }

   notice(value, key, oldValue = this[key]) {
    let lists = this.observers[key]
    if (lists == null) {
      return
    }

    lists.map((observer) => {
      observer.subscribe(this[key], value, key, this)
    })
  }
}
